package mos.mobile.uniandes.gymar.models.exercises

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.util.Log
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import mos.mobile.uniandes.gymar.entities.ExerciseEntity
import mos.mobile.uniandes.gymar.entities.RoutineExerciseEntity
import mos.mobile.uniandes.gymar.helpers.FireBaseDBConstants
import mos.mobile.uniandes.gymar.helpers.RoomDBConstants
import mos.mobile.uniandes.gymar.models.GymDataBase
import mos.mobile.uniandes.gymar.pojos.RoutineExercise
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception
import java.util.HashMap

class ExercisesByRoutineRepository(context: Context): ExerciseRepository<RoutineExercise> {


//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------

    private val TAG: String = ExercisesByRoutineRepository::class.java.simpleName

    private val gymFireStore: FirebaseFirestore

    private val gymDB: GymDataBase

    init {
        gymDB = GymDataBase.getInstance(context)!!
        gymFireStore = FirebaseFirestore.getInstance()
    }

//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------

    private var exercises: MutableLiveData<MutableList<RoutineExercise>> = MutableLiveData()

    private var exercisesLive: MutableLiveData<MutableList<RoutineExercise>> = MutableLiveData()

    private lateinit var listenerFirestore: ListenerRegistration

//------------------------------------------------------------------------
// PUBLIC FUNCTIONS
//------------------------------------------------------------------------

    /**
     * Gets the exercises according to a specific routine
     */
    override fun getExercises(conectivity: Boolean, searchParam: String): LiveData<MutableList<RoutineExercise>> {
        Log.d(TAG, "Getting Exercises from repository")
        Log.d(TAG, "Conectivity? $conectivity")

        if (!conectivity){
            if(exercisesLive.value == null) {
                Log.d(TAG, "Fetching Locally")
                exercisesLive.postValue(mutableListOf<RoutineExercise>())
                loadExcersisesFromStorage(searchParam)
            }
            return exercisesLive
        }
        else {
            if(exercises.value == null){
                Log.d(TAG, "Fetching Remotely")
                exercises.postValue(mutableListOf<RoutineExercise>())
                fetchExercisesByRoutine(searchParam)
            }
            return exercises
        }
    }

    /**
     *  Removes the listener when it is no longer needed
     */
    override fun detachPosibleListeners() {
        if(::listenerFirestore.isInitialized){
            listenerFirestore.remove()
        }
    }


//------------------------------------------------------------------------
// PRIVATE FUNCITONS
//------------------------------------------------------------------------

    /**
     * Returns a LiveData list with the exercises from Room, depending on the search type (cateogry or routine)
     */
    private fun loadExcersisesFromStorage(searchParam: String){
        doAsync {
            val ejercicios: List<ExerciseEntity> = gymDB.exerciseDao().getAllExerciseEntitiesByRoutineId(searchParam)//getAllExerciseEntitiesByRoutineId(searchParam)
            var rutinas: List<RoutineExerciseEntity>
            var resultado: ArrayList<RoutineExercise> = ArrayList()

            ejercicios.forEach { exerciseEntity: ExerciseEntity ->
                rutinas = gymDB.routineExerciseDao().getAllRoutineExerciseEntitiesByRoutineIdAndExerciseId(searchParam, exerciseEntity.dosId)

                if(rutinas != null && rutinas.isNotEmpty()){
                    resultado.add(RoomDBConstants.createRoutineExerciseEntity(exerciseEntity, rutinas[0]))
                }
            }

            uiThread {
                Log.d(TAG, "resultado: $resultado")
                exercisesLive.postValue(resultado)
            }
        }
    }

    /**
     * Fetches Exercises from Firestore that belong to the category of the parameter
     */
    private fun fetchExercisesByRoutine(searchParam: String){
        gymFireStore.collection(FireBaseDBConstants.ROUTINES_TABLE).document(searchParam).get().addOnCompleteListener { task ->
            if(task.isSuccessful){
                val document = task.result
                if(document != null){

                    //obtiene los datos de la rutina
                    var routineHashMap = document.data!! as HashMap

                    //obtiene los datos del arreglo de ejercicios
                    var listRoutineExercise: ArrayList<Any> = routineHashMap.get(FireBaseDBConstants.ROUTINES_EXERCISES) as ArrayList<Any>

                    for (entry in listRoutineExercise) {

                        //Obtiene los componentes de cada elemento de la lista
                        var componentHashMap = entry as HashMap<String, *>
                        var repeticiones: Long = componentHashMap.get(FireBaseDBConstants.ROUTINES_REPETICIONES) as Long
                        var series: Long = componentHashMap.get(FireBaseDBConstants.ROUTINES_SERIES) as Long

                        //documento de referencia al ejercicio
                        var ejercicio: DocumentReference = componentHashMap.get(FireBaseDBConstants.ROUTINES_EXERCISE) as DocumentReference

                        ejercicio.get().addOnCompleteListener { secondTask ->
                            if(secondTask.isSuccessful){
                                val secondDocument = secondTask.result

                                if(secondDocument != null){
                                    exercises.value?.add(RoutineExercise(repeticiones, series, FireBaseDBConstants.createExerciseEntity(secondDocument)))
                                    exercises.postValue(exercises.value)
                                }
                                else{
                                    throw java.lang.Exception(secondTask.exception)
                                }
                            }
                            else{
                                throw java.lang.Exception(secondTask.exception)
                            }
                        }
                    }
                }
                else{
                    throw Exception(task.exception)
                }
            }
            else{
                throw Exception(task.exception)
            }
        }
    }


}