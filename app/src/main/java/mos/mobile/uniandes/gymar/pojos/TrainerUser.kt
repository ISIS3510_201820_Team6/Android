package mos.mobile.uniandes.gymar.pojos

import java.util.Date

data class TrainerUser(var cliente: String, var imagen: String,
                       var ejercicio:String, var fechaSolicitud: Date,
                       var realizada: Boolean, var tId: String,
                       var cId: String, var id: String, var condiciones: String, var nombreEntrenador: String) {
}