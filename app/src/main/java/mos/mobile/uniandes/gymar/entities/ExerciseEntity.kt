package mos.mobile.uniandes.gymar.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

//Entity that describes Exercises in the DB
@Parcelize
@Entity(tableName = "ejercicios")
data class ExerciseEntity(
        @PrimaryKey @ColumnInfo(name = "id") var id: String,
        @ColumnInfo(name = "dosId") var dosId: String,
        @ColumnInfo(name = "nombre") var nombre: String,
        @ColumnInfo(name = "categoria") var categoria: String,
        @ColumnInfo(name = "descripcion") var descripcion: String,
        @ColumnInfo(name = "iconoAndroid") var iconoAndroid: String,
        @ColumnInfo(name = "iconoIOS") var iconoIOS: String,
        @ColumnInfo(name = "foto") var foto: String,
        @ColumnInfo(name = "video") var video: String,
        @ColumnInfo(name = "ar") var ar: String,
        @ColumnInfo(name = "zona") var zona: String) : Parcelable{
}