package mos.mobile.uniandes.gymar.helpers.chartFormatters

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.IAxisValueFormatter

class MyBodyAxisValueFormatter(var values: Array<String>): IAxisValueFormatter {

    override fun getFormattedValue(value: Float, axis: AxisBase?): String {
        return values[value.toInt() - 1]
    }

}