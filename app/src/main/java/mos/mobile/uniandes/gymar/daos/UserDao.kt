package mos.mobile.uniandes.gymar.daos

import android.arch.persistence.room.*
import mos.mobile.uniandes.gymar.entities.UserEntity

@Dao
public interface UserDao {
    @Insert
    fun insertUser(user: UserEntity)

    @Query("SELECT * FROM users WHERE userId = :userId")
    fun fetchUser(userId: String): UserEntity

    @Query("SELECT * FROM users")
    fun fetchAllUsers(): MutableList<UserEntity>

    @Delete
    fun delete(user: UserEntity)

    @Update
    fun updateUser(user: UserEntity)
}