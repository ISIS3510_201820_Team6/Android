package mos.mobile.uniandes.gymar.helpers

import mos.mobile.uniandes.gymar.entities.ExerciseEntity
import mos.mobile.uniandes.gymar.entities.RoutineExerciseEntity
import mos.mobile.uniandes.gymar.pojos.RoutineExercise

class RoomDBConstants {

    companion object {

        fun createRoutineExerciseEntity(exercise: ExerciseEntity, routineExercise: RoutineExerciseEntity): RoutineExercise{
            return RoutineExercise(numeroSeries = routineExercise.series, numeroRepeticionesPorSerie = routineExercise.repetitions, ejercicio = exercise)
        }
    }
}