package mos.mobile.uniandes.gymar

import android.app.AlertDialog
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.util.Log
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import mos.mobile.uniandes.gymar.activities.trainers.TrainerMainActivity
import mos.mobile.uniandes.gymar.helpers.ConectivityHelper

/**
 * A login screen that offers login via email/password.
 */
class LoginActivity : AppCompatActivity(){

    private lateinit var mAuth: FirebaseAuth
    private val db = FirebaseFirestore.getInstance().collection("usuarios")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        mAuth = FirebaseAuth.getInstance()

        //Set the button function
        findViewById<Button>(R.id.email_sign_in_button).setOnClickListener { onSignIn() }
    }

    override fun onStart() {
        super.onStart()
        var currentUser = mAuth.currentUser
        if (currentUser != null){
            Log.d(LoginActivity::class.java.simpleName, currentUser.toString())
            checkUserRole()
        }
    }

    fun onSignIn() {
        if(ConectivityHelper.checkOverallConectivity(this)){
            var email = findViewById<AutoCompleteTextView>(R.id.email).text.toString()
            var password = findViewById<EditText>(R.id.password).text.toString()
            if (email.isEmpty() || password.isEmpty()) {
                Snackbar.make(findViewById(R.id.login_layout), "Debes ingresar tu usuario y tu contraseña", Snackbar.LENGTH_INDEFINITE).show()
            } else {
                mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        checkUserRole()
                    }
                    else{
                        Snackbar.make(findViewById(R.id.login_layout), "Usuario o contraseña incorrectas", Snackbar.LENGTH_INDEFINITE).show()
                    }

                }
            }
        }
        else {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Sin Internet")
            builder.setMessage("Para iniciar sesión necesitas internet, por favor revisa tu conexión y intenta más tarde.")

            val dialog = builder.create()
            dialog.show()
        }
    }

    fun checkUserRole(){
        var currentUser = mAuth.currentUser
        if (currentUser != null) {
            val uId = currentUser.uid
            var role: String? = ""
            val db2 = db.document(uId)
            db2.get().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val document = task.result
                    role = document?.getString("rol")
                    launchActivity(role)
                }
            }

        } else {
            Snackbar.make(findViewById(R.id.login_layout), "Usuario o contraseña incorrectos", Snackbar.LENGTH_LONG).show()
        }

    }

    fun launchActivity(role: String?){
        if (!role.equals("entrenador")) {
            //Lanzar Main Activity
            var intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        } else {
            var intent = Intent(this, TrainerMainActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        val a = Intent(Intent.ACTION_MAIN)
        a.addCategory(Intent.CATEGORY_HOME)
        a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(a)
    }


}
