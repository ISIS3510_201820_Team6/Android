package mos.mobile.uniandes.gymar.models.statistics

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import mos.mobile.uniandes.gymar.R
import mos.mobile.uniandes.gymar.helpers.FireBaseDBConstants
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.ref.WeakReference

class MyBodyRepository(){

//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------

    private val TAG: String = MyBodyRepository::class.java.simpleName

    private val gymFireStore: FirebaseFirestore

    private var mAuth: FirebaseAuth

    init {
        gymFireStore = FirebaseFirestore.getInstance()
        mAuth = FirebaseAuth.getInstance()
    }

//------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------

    private var data: MutableLiveData<BarDataSet?> = MutableLiveData()

    private lateinit var listenerFirestore: ListenerRegistration

//------------------------------------------------------------------------
// PUBLIC FUNCITONS
//------------------------------------------------------------------------

    fun getMyBodyData( connectivity: Boolean):LiveData<BarDataSet?>{
        if(connectivity){
            fetchMyBodyStatistics( )
        }
        return data
    }

    fun detachPosibleListeners(){
        if(::listenerFirestore.isInitialized){
            listenerFirestore.remove()
        }
    }

//------------------------------------------------------------------------
// PRIVATE FUNCITONS
//------------------------------------------------------------------------

    private fun fetchMyBodyStatistics() {
        var currentUser = mAuth.currentUser
        if (currentUser != null){
            var query: Query = gymFireStore.collection(FireBaseDBConstants.BODY_STATISTICS_TABLE)
                    .whereEqualTo(FireBaseDBConstants.BODY_STATISTICS_USER, gymFireStore.collection(FireBaseDBConstants.USERS_TABLE).document(currentUser.uid))

            listenerFirestore = query.addSnapshotListener(EventListener<QuerySnapshot>{ querySnapshot, exception ->
                if(exception != null){
                    Log.d(TAG, "Error en el SnapshotListener!!")
                    return@EventListener
                }
                else{
                    doAsync {

                        var barEntries: ArrayList<BarEntry> = ArrayList()
                        querySnapshot?.forEach{document ->
                            barEntries.add(BarEntry(1F, (document.get(FireBaseDBConstants.BODY_STATISTICS_BRAZO) as ArrayList<Float>).toFloatArray()))
                            barEntries.add(BarEntry(2F, (document.get(FireBaseDBConstants.BODY_STATISTICS_ANTEBRAZO) as ArrayList<Float>).toFloatArray()))
                            barEntries.add(BarEntry(3F, (document.get(FireBaseDBConstants.BODY_STATISTICS_MUSLO) as ArrayList<Float>).toFloatArray()))
                            barEntries.add(BarEntry(4F, (document.get(FireBaseDBConstants.BODY_STATISTICS_PANTORRILLA) as ArrayList<Float>).toFloatArray()))
                            barEntries.add(BarEntry(5F, (document.get(FireBaseDBConstants.BODY_STATISTICS_TORAX) as ArrayList<Float>).toFloatArray()))
                        }

                        var dataSet: BarDataSet = BarDataSet(barEntries, "Condición Física")
                        dataSet.setDrawIcons(false)
                        dataSet.stackLabels = arrayOf("Inicial", "Actual", "Meta")

                        uiThread {
                            data.postValue(dataSet)
                        }
                    }
                }
            })
        }
    }



}