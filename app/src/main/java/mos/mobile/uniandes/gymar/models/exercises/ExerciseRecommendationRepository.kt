package mos.mobile.uniandes.gymar.models.exercises

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import mos.mobile.uniandes.gymar.entities.ExerciseEntity
import mos.mobile.uniandes.gymar.helpers.FireBaseDBConstants
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class ExerciseRecommendationRepository : ExerciseRepository<ExerciseEntity> {


//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------

    private val TAG: String = ExerciseRecommendationRepository::class.java.simpleName

    private val gymFireStore: FirebaseFirestore

    init {
        gymFireStore = FirebaseFirestore.getInstance()
    }

//------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------

    private var exercises: MutableLiveData<MutableList<ExerciseEntity>> = MutableLiveData()

    private lateinit var listenerFirestore: ListenerRegistration

//------------------------------------------------------------------------
// PUBLIC FUNCTIONS
//------------------------------------------------------------------------

    /**
     * Gets the exercises recommended for the user
     */
    override fun getExercises(conectivity: Boolean, searchParam: String): LiveData<MutableList<ExerciseEntity>> {
        Log.d(TAG, "Getting Exercises")
        if(conectivity){
            if(exercises.value == null){
                Log.d(TAG, "Fetching Remotely")
                fetchRecommendeExercises( )
            }
        }
        return exercises
    }

    /**
     * Removes the listener when it is no longer needed
     */
    override fun detachPosibleListeners() {
        if(::listenerFirestore.isInitialized){
            listenerFirestore.remove()
        }
    }

//------------------------------------------------------------------------
// PRIVATE FUNCITONS
//------------------------------------------------------------------------

    /**
     * Fetches Exercises from Firestore that belong to the category of the parameter
     */
    private fun fetchRecommendeExercises( ) {
        var query: Query = gymFireStore.collection(FireBaseDBConstants.EXERCISES_TABLE)
                .limit(5)


        listenerFirestore = query.addSnapshotListener(EventListener<QuerySnapshot> { querySnapshot, exception ->

            if (exception != null) {
                Log.d(TAG, "Error en el SnapshotListener!!")
                return@EventListener
            } else {
                doAsync {
                    var exerciseList: ArrayList<ExerciseEntity> = ArrayList<ExerciseEntity>()
                    querySnapshot?.forEach { document ->
                        exerciseList.add(FireBaseDBConstants.createExerciseEntity(document))
                    }

                    uiThread {
                        exercises.postValue(exerciseList)
                    }
                }
            }
        })

    }
}