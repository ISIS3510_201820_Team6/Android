package mos.mobile.uniandes.gymar.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

//Entity that describes the Routine DB scheme
@Entity(tableName = "users")
data class UserEntity(
        @PrimaryKey
        @ColumnInfo(name = "userId") var id: String,
        @ColumnInfo(name = "name") var name: String,
        @ColumnInfo(name = "age") var age: Int?,
        @ColumnInfo(name = "weight") var weight: Double?,
        @ColumnInfo(name = "bodyFat") var bodyFat: Double?,
        @ColumnInfo(name = "maxHr") var maxHr: Int?,
        @ColumnInfo(name = "imc") var imc: Double?,
        @ColumnInfo(name = "height") var height: Int?,
        @ColumnInfo(name = "imagen") var image: String?

){
    //TODO: Implement Class Constructor
}
