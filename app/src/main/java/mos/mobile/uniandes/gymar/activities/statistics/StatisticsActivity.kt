package mos.mobile.uniandes.gymar.activities.statistics

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.ActivityInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.LineData
import mos.mobile.uniandes.gymar.R
import mos.mobile.uniandes.gymar.viewModels.StatisticsViewModel

class StatisticsActivity : AppCompatActivity() {

//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------
    /**
     * TAG para el uso de Logs en la aplicacion
     */
    private val TAG = StatisticsActivity::class.java.simpleName

//------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------

    /**
     * ViewModel for the activity
     */
    private lateinit var viewModel: StatisticsViewModel

    /**
     * Graph
     */
    private lateinit var fatChart: LineChart

    /**
     * Graph
     */
    private lateinit var weightChart: LineChart

    /**
     * Graph
     */
    private lateinit var perimeterChart: LineChart

//------------------------------------------------------------------------
// LIFECYCLE FUNCTIONS
//------------------------------------------------------------------------

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statistics)
        var toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.setTitle("Estadísticas")
        setSupportActionBar(toolbar)
        //Enable back button on action bar.
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        viewModel = ViewModelProviders.of(this).get(StatisticsViewModel::class.java)

        fatChart = findViewById<LineChart>(R.id.activity_statistics_fatgraph)
        weightChart = findViewById<LineChart>(R.id.activity_statistics_weightgraph)
        perimeterChart = findViewById<LineChart>(R.id.activity_statistics_perimetergraph)
    }

    override fun onStart() {
        super.onStart()
        viewModel.configureData(this.applicationContext)
        viewModel.getFatStatistics(this.applicationContext).observe(this, Observer { data ->
            if(data != null){
                Log.d(TAG, "Fat Data")
                fatChart.data = LineData(data)
                fatChart.invalidate()
            }
        })
        viewModel.getWeightStatistics(this.applicationContext).observe(this, Observer { data ->
            if(data != null){
                Log.d(TAG, "Weight Data")
                weightChart.data = LineData(data)
                weightChart.invalidate()
            }
        })
        viewModel.getPerimeterStatistics(this.applicationContext).observe(this, Observer { data ->
            if(data != null){
                Log.d(TAG, "Perimeter Data")
                perimeterChart.data = LineData(data)
                perimeterChart.invalidate()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        requestedOrientation =  ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    override fun onPause() {
        super.onPause()
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
    }

    override fun onStop() {
        super.onStop()
        viewModel.detachListeners()
    }

//------------------------------------------------------------------------
// SETUP FUNCTIONS
//------------------------------------------------------------------------

    /**
     * Helper function for back button in action bar
     */
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id: Int? = item?.itemId
        if(id == android.R.id.home){
            this.finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
