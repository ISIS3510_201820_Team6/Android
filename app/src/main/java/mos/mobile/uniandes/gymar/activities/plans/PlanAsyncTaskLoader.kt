package mos.mobile.uniandes.gymar.activities.plans

import android.arch.lifecycle.LiveData
import android.content.Context
import android.support.v4.content.AsyncTaskLoader
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import mos.mobile.uniandes.gymar.entities.PlanEntity
import mos.mobile.uniandes.gymar.entities.RoutineEntity
import mos.mobile.uniandes.gymar.entities.RoutineExerciseEntity
import mos.mobile.uniandes.gymar.helpers.ConectivityHelper
import mos.mobile.uniandes.gymar.helpers.FireBaseDBConstants
import mos.mobile.uniandes.gymar.models.GymDataBase
import org.jetbrains.anko.doAsync

class PlanAsyncTaskLoader(context: Context): AsyncTaskLoader<PlanEntity>(context) {

    private val TAG: String = PlanAsyncTaskLoader::class.java.simpleName

    private val gymFireStore: FirebaseFirestore

    private var mAuth: FirebaseAuth

    private val gymDB: GymDataBase

    init {
        gymDB = GymDataBase.getInstance(context)!!
        mAuth = FirebaseAuth.getInstance()
        gymFireStore = FirebaseFirestore.getInstance()
    }

    override fun loadInBackground(): PlanEntity? {
        val currentUser = mAuth.currentUser
        if(currentUser != null && ConectivityHelper.checkOverallConectivity(context)){
            gymFireStore.collection(FireBaseDBConstants.PLANS_USERS_TABLE)
                .whereEqualTo(FireBaseDBConstants.PLANS_USERS_USER, gymFireStore.collection(FireBaseDBConstants.USERS_TABLE).document(currentUser.uid))
                .orderBy(FireBaseDBConstants.PLANS_USERS_BEGIN, Query.Direction.DESCENDING)
                .limit(1)
                .addSnapshotListener(EventListener<QuerySnapshot> { querySnapshot, exception ->

                    if (exception != null) {
                        Log.e(TAG, exception.message)
                        return@EventListener
                    }
                    else{
                        querySnapshot?.forEach { document ->

                            doAsync {
                                val idPlanFireStore = document.getDocumentReference(FireBaseDBConstants.PLANS_USERS_PLAN)?.id!!
                                val plan: PlanEntity? = gymDB.planDao().getPlansById(idPlanFireStore)

                                if(plan == null){

                                    gymDB.routineExerciseDao().deleteAll()
                                    gymDB.exerciseDao().deleteAll()
                                    gymDB.routineDao().deleteAll()
                                    gymDB.planDao().deleteAll()

                                    document.getDocumentReference(FireBaseDBConstants.PLANS_USERS_PLAN)?.get()?.addOnCompleteListener { task ->
                                        var result = task.result

                                        if(result != null){
                                            var planEntity = FireBaseDBConstants.createPlanEntity(result)

                                            doAsync {
                                                gymDB.planDao().insert(planEntity)
                                            }

                                            val listRoutines: ArrayList<DocumentReference?> = result.get(FireBaseDBConstants.PLAN_ROUTINES) as ArrayList<DocumentReference?>
                                            persistRoutines(planEntity, listRoutines)
                                        }
                                        else{
                                            Log.e(TAG, "Error en la carga de datos! Resultado inexiste")
                                        }
                                    }
                                }
                                else{
                                    Log.d(TAG, "Plan existente")
                                }
                            }
                        }
                    }
                })
        }
        else{
            Log.d(TAG, "Connectivity: ${ConectivityHelper.checkOverallConectivity(context)} & current user: $currentUser")
        }
        return null
    }

    override fun onStartLoading() {
        forceLoad()
    }


    /**
     * A partir de un plan y una lista de rutinas, se persiste la informacion en Room
     */
    private fun persistRoutines(planEntity: PlanEntity, listRoutines: ArrayList<DocumentReference?>){
        listRoutines.forEach { r ->
            r!!.get().addOnCompleteListener{secondTask ->
                if(secondTask.isSuccessful){
                    val secondDocument = secondTask.result
                    if (secondDocument != null){
                        val  routine: RoutineEntity = FireBaseDBConstants.createRoutineEntity(secondDocument, planEntity)

                        doAsync {
                            gymDB.routineDao().insert(routine)
                        }

                        //obtiene los datos de la rutina
                        var map = secondDocument.data!!as HashMap

                        //obtiene los datos del arreglo de ejercicios
                        var listRoutineExercise: ArrayList<Any> = map.get(FireBaseDBConstants.ROUTINES_EXERCISES) as ArrayList<Any>

                        listRoutineExercise.forEach { entry ->
                            //Obtiene los componentes de cada elemento de la lista
                            var algo = entry as HashMap<String, *>

                            //documento de referencia al ejercicio
                            persistExercises(algo.get(FireBaseDBConstants.ROUTINES_EXERCISE) as DocumentReference, routine,
                                    algo.get(FireBaseDBConstants.ROUTINES_REPETICIONES) as Long, algo.get(FireBaseDBConstants.ROUTINES_SERIES) as Long)

                        }
                    }
                    else{
                        Log.e(TAG, "Error en la carga!! Rutina no encontrada!")
                    }
                }
                else{
                    Log.e(TAG, secondTask.exception?.message)
                }
            }
        }
    }

    /**
     * A partir de una lista de ejercicios, se eprsiste la informacion en Room
     */
    private fun persistExercises(ejercicio: DocumentReference, routine: RoutineEntity, repetitions: Long, series: Long){
        ejercicio.get().addOnCompleteListener { secondTask ->
            if(secondTask.isSuccessful){
                val secondDocument = secondTask.result

                if(secondDocument != null){
                    Log.d(TAG, secondDocument.id)
                    var unExerciseEntity = FireBaseDBConstants.createExerciseEntity(secondDocument)
                    var unRoutineExerciseEntity = RoutineExerciseEntity(0, routine.id, unExerciseEntity.id, repetitions, series)
                    unExerciseEntity.id = secondDocument.id

                    doAsync {
                        gymDB.exerciseDao().insert(unExerciseEntity)
                        gymDB.routineExerciseDao().insert(unRoutineExerciseEntity)

                    }
                }
                else{
                    throw java.lang.Exception(secondTask.exception)
                }
            }
            else{
                throw java.lang.Exception(secondTask.exception)
            }
        }
    }
}