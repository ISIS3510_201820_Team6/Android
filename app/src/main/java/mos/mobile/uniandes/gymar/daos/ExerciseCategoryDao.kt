package mos.mobile.uniandes.gymar.daos

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import mos.mobile.uniandes.gymar.entities.ExerciseCategoryEntity

@Dao
interface ExerciseCategoryDao: CommonDao<ExerciseCategoryEntity> {

    @Query(value = "SELECT * FROM categorias")
    fun getAllExerciseCategories(): List<ExerciseCategoryEntity>?

    @Query(value = "DELETE FROM categorias")
    fun deleteAll()

    @Transaction
    fun insertAndUpdateExerciseCategories(categories: ArrayList<ExerciseCategoryEntity>){
        categories.forEach { cat ->
            insert(cat)
        }
    }
}