package mos.mobile.uniandes.gymar.entities

import android.arch.persistence.room.*

//Entity that describes the Routine's Exercise DB scheme
@Entity(tableName = "rutina_ejercicio",
        foreignKeys = [ForeignKey(
                entity = RoutineEntity::class,
                parentColumns = ["id"],
                childColumns = ["routineId"],
                onDelete = ForeignKey.CASCADE
        ), ForeignKey(
                entity = ExerciseEntity::class,
                parentColumns = ["id"],
                childColumns = ["exerciseId"],
                onDelete = ForeignKey.CASCADE
        )],
        indices = [Index(
                    value = "routineId",
                    name = "routinesi"),
            Index(value = "exerciseId",
                    name = "exercisesi")])
data class RoutineExerciseEntity(
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Long,
        @ColumnInfo(name = "routineId") var routineId: String,
        @ColumnInfo(name = "exerciseId") var exerciseId: String,
        @ColumnInfo(name = "repetitions") var repetitions: Long,
        @ColumnInfo(name = "series") var series: Long) {
}
