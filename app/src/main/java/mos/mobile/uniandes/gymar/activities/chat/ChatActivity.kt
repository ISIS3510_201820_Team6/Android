package mos.mobile.uniandes.gymar.activities.chat

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.TextInputEditText
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import mos.mobile.uniandes.gymar.R
import mos.mobile.uniandes.gymar.pojos.TextMessageItem
import mos.mobile.uniandes.gymar.viewModels.MessageViewModel
import java.util.HashMap
import android.support.v4.os.HandlerCompat.postDelayed
import mos.mobile.uniandes.gymar.helpers.ConectivityHelper


class ChatActivity : AppCompatActivity() {

    lateinit var recyclerView: RecyclerView

    lateinit var adapter: MessagesAdapter

    lateinit var viewModel : MessageViewModel

    lateinit var linearLayoutManager: LinearLayoutManager

    lateinit var chatId: String

    lateinit var nombreEntrenador: String

    lateinit var nombreCliente: String

    lateinit var idEntrenador: String

    lateinit var idCliente: String

    private val chatDB = FirebaseFirestore.getInstance().collection("Messages")

    private lateinit var mAuth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        mAuth = FirebaseAuth.getInstance()
        var usuario = mAuth.currentUser
        var title = "Chat"
        var toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title= title
        setSupportActionBar(toolbar)
        chatId = intent.getStringExtra("CHAT_ID")
        //Enable back button on action bar.
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        // Recycler View Configuration
        recyclerView = findViewById(R.id.chat_messages_recycler)
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        adapter = MessagesAdapter(mutableListOf<TextMessageItem>(), this, usuario!!.uid)
        recyclerView.adapter = adapter

        recyclerView.addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
            override fun onLayoutChange(view: View, left: Int, top: Int, right: Int, bottom: Int,
                                        oldLeft: Int, oldTop: Int, oldRight: Int, oldBottom: Int) {
                if(bottom < oldBottom){
                    var items = recyclerView.adapter?.itemCount
                    if(items != null){
                        var bottomP = items - 1
                        if(bottomP > 0 || bottomP == 0){
                            recyclerView.postDelayed({ recyclerView.smoothScrollToPosition(bottomP) }, 250)
                        }

                    }
                }
            }
        })

        //ImageView configuration
        var imageView = findViewById<ImageView>(R.id.send_message_button)
        imageView.setOnClickListener { sendMessage() }

        viewModel = ViewModelProviders.of(this).get(MessageViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        viewModel.getMessages(this.applicationContext, chatId).observe(this, Observer{mensajes ->
            if(mensajes != null && !mensajes.isEmpty()){
                adapter.setMessages(mensajes)
            }
            else{
            }
        })



    }

    //Helper function for back button in action bar
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id: Int? = item?.itemId
        if(id == android.R.id.home){
            this.finish()
        }
        return super.onOptionsItemSelected(item)
    }

    fun sendMessage(){
        if(ConectivityHelper.checkOverallConectivity(this)){
            val textInput = findViewById<TextInputEditText>(R.id.message_input)
            val mensaje = textInput.text.toString()
            val senderId = mAuth.currentUser?.uid
            val idChat = chatId
            val fecha = java.util.Date()
            var solicitud = HashMap<String, Any?>()
            solicitud.put("idChat", idChat)
            solicitud.put("idSender", senderId)
            solicitud.put("mensaje", mensaje)
            solicitud.put("fecha", fecha)

            chatDB.add(solicitud)

            textInput.setText("", TextView.BufferType.EDITABLE)
        }
        else{
            Snackbar.make(findViewById<ImageView>(R.id.send_message_button), "No se pudo enviar, no hay conexión a internet", Snackbar.LENGTH_LONG).show()
        }

    }
}