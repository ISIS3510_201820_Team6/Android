package mos.mobile.uniandes.gymar.activities.chat

import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.LinearLayoutCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import mos.mobile.uniandes.gymar.R
import mos.mobile.uniandes.gymar.pojos.TextMessageItem

class MessagesAdapter(val messages: MutableList<TextMessageItem>, val activity: ChatActivity, var userId: String) : RecyclerView.Adapter<MessagesAdapter.ViewHolder>() {

    private lateinit var recyclerView: RecyclerView
    override fun onCreateViewHolder(parentContext: ViewGroup, position: Int): ViewHolder {
        val view: View = LayoutInflater.from(parentContext.context).inflate(R.layout.item_message, parentContext, false)
        return ViewHolder(view, activity)
    }

    override fun onAttachedToRecyclerView(pRecyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(pRecyclerView)
        recyclerView = pRecyclerView
    }
    override fun getItemCount(): Int {
        return messages.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int){
        val message = messages[p1]
        p0.bindMessage(message, userId)
    }

    fun setMessages(lista: MutableList<TextMessageItem>){
        messages.clear()
        messages.addAll(lista)
        notifyDataSetChanged()
        var number = itemCount
        if(number > 0 || number == 0){
            recyclerView.smoothScrollToPosition(itemCount - 1)
        }

    }

    class ViewHolder (itemView: View, val activity: ChatActivity): RecyclerView.ViewHolder(itemView) {

        val message = itemView.findViewById<TextView>(R.id.mensaje)

        val fecha = itemView.findViewById<TextView>(R.id.fecha)

        val layout = itemView.findViewById<LinearLayoutCompat>(R.id.chat_message_layout)

        fun bindMessage(mensaje: TextMessageItem, userId: String){
            fecha.text = mensaje.date
            message.text = mensaje.message
            if(!(mensaje.idSender == (userId)) ){
                layout.background = ResourcesCompat.getDrawable(activity.resources, R.drawable.drawable_chat_rect2, null)
                layout.layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.START)

            }
        }
    }
}