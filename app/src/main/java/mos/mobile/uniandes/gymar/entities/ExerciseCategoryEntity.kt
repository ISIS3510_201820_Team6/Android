package mos.mobile.uniandes.gymar.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "categorias")
data class ExerciseCategoryEntity(
        @PrimaryKey @ColumnInfo(name = "id") var id: String,
        @ColumnInfo(name = "nombre") var nombre: String,
        @ColumnInfo(name = "icono_android") var iconoAndroid: String,
        @ColumnInfo(name = "iconoIOS") var iconoIOS: String): Parcelable{


    @Ignore
    constructor(): this("", "", "", "")
}