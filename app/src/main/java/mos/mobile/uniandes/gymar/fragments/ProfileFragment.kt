package mos.mobile.uniandes.gymar.fragments


import android.app.AlertDialog
import android.arch.persistence.room.Room
import android.content.ContentValues.TAG
import android.content.Intent
import android.content.pm.ActivityInfo
import android.databinding.DataBindingUtil
import android.os.AsyncTask

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.mikhaellopez.circularimageview.CircularImageView
import mos.mobile.uniandes.gymar.R
import mos.mobile.uniandes.gymar.activities.statistics.MyBodyActivity
import mos.mobile.uniandes.gymar.activities.statistics.StatisticsActivity
import mos.mobile.uniandes.gymar.databases.UserDatabase
import mos.mobile.uniandes.gymar.databinding.FragmentProfileBinding
import mos.mobile.uniandes.gymar.entities.UserEntity
import mos.mobile.uniandes.gymar.helpers.ConectivityHelper
import mos.mobile.uniandes.gymar.pojos.User
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.startActivity
import java.lang.Exception


class ProfileFragment : Fragment() {

    private val db = FirebaseFirestore.getInstance().collection("usuarios")

    private lateinit var mAuth: FirebaseAuth

    companion object {
        var database: UserDatabase? = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        database = Room.databaseBuilder(activity?.applicationContext!!, UserDatabase::class.java, "user-database-db").build()
        //Inflate layout with data binding
        val binding = DataBindingUtil.inflate<FragmentProfileBinding>(inflater, R.layout.fragment_profile, container, false)

        mAuth = FirebaseAuth.getInstance()

        //Check Connectivity
        //
        val userId: String = mAuth.currentUser!!.uid
        val db2 = db.document(userId)
        if(ConectivityHelper.checkOverallConectivity(context!!)){
            db2.get().addOnCompleteListener { task ->

                if(task.isSuccessful){
                    val document = task.result
                    if (document != null) {
                        val nombre = document.getString("nombre")
                        val apellido = document.getString("apellido")
                        val edad = document.getDouble("Edad")
                        val estatura = document.getDouble("Estatura")
                        val imc = document.getDouble("IMC")
                        val grasa = document.getDouble("grasaCorporal")
                        val hr = document.getDouble("maxHR")
                        val peso = document.getDouble("peso")
                        val imagen = document.getString("imagen")
                        val name = "$nombre $apellido"

                        var theUser = User(name, grasa, peso, hr?.toInt(), imc, edad?.toInt(), estatura?.toInt(), imagen)
                        binding.user = theUser
                        val image = binding.root.findViewById<CircularImageView>(R.id.profile_image)
                        var glideOptions = RequestOptions().placeholder(R.drawable.person_orange_24dp).error(R.drawable.person_orange_24dp)
                        Glide.with(binding.root).setDefaultRequestOptions(glideOptions).load(imagen).into(image)
                        addOrUpdateData(theUser)
                    } else {
                        throw Exception("No such document")
                    }
                }
                else{
                    throw Exception(task.exception)
                }
            }
            
        }
        else{
            var user = RetrieveUserFromStorage().execute().get()
            Log.d(TAG,user.toString())
            if(user == null){
                val builder = AlertDialog.Builder(activity)
                builder.setTitle("Primer uso")
                builder.setMessage("Necesitamos descargar datos la primera vez que ingresas a la app, por favor revisa tu conexión a internet y intenta más tarde")

                val dialog = builder.create()
                dialog.show()
            }
            else{
                binding.user = user
                val imagen = user.image
                val image = binding.root.findViewById<CircularImageView>(R.id.profile_image)
                var glideOptions = RequestOptions().placeholder(R.drawable.person_orange_24dp).error(R.drawable.person_orange_24dp)
                Glide.with(binding.root).setDefaultRequestOptions(glideOptions).load(imagen).into(image)
                Toast.makeText(activity, "Modo offline", Toast.LENGTH_LONG).show()
            }

        }
        // Return the view
        database?.close()

        //Set listeners
        var button1 = binding.root.findViewById<Button>(R.id.my_physical_condition_button)
        button1?.setOnClickListener { launchConditioningActivity() }

        //Set listeners
        var button2 = binding.root.findViewById<Button>(R.id.my_body_button)
        button2?.setOnClickListener { launchStatisticsActivity()}

        return binding.root
    }


    private fun addOrUpdateData(mUser: User){
        doAsync {
            val users = database?.userDao()?.fetchAllUsers()
            Log.d(TAG, users.toString() +  "User Debug")
            if (users!!.isEmpty()){
                var mUserEntity = UserEntity("1",mUser.name, mUser.age, mUser.weight, mUser.bodyFat, mUser.maxHR, mUser.imc, mUser.height, mUser.image)
                database?.userDao()?.insertUser(mUserEntity)
            }
            else{
                var mUserEntity = UserEntity("1",mUser.name, mUser.age, mUser.weight, mUser.bodyFat, mUser.maxHR, mUser.imc, mUser.height, mUser.image)
                database?.userDao()?.updateUser(mUserEntity)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    override fun onPause() {
        super.onPause()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
    }



    private class RetrieveUserFromStorage: AsyncTask<String,Int,User?>(){

        override fun doInBackground(vararg params: String?): User? {
            var fetchedUser: User? = null
            val users = database?.userDao()?.fetchAllUsers()
            if (users!!.isEmpty()) {

            } else {

                var u = users[0]
                fetchedUser = User(u.name, u.bodyFat, u.weight, u.maxHr, u.imc, u.age, u.height, u.image)
            }
            return fetchedUser

        }

    }

    private fun launchConditioningActivity(){
        if(ConectivityHelper.checkOverallConectivity(context!!)){
            activity?.startActivity<MyBodyActivity>()
        }
        else{
            val builder = AlertDialog.Builder(activity)
            builder.setTitle("Sin Internet")
            builder.setMessage("Para acceder a esta función necesitas internet, por favor revisa tu conexión y intenta más tarde.")

            val dialog = builder.create()
            dialog.show()
        }
    }

    private fun launchStatisticsActivity(){
        if(ConectivityHelper.checkOverallConectivity(context!!)){
            val intent = Intent(context, StatisticsActivity::class.java)
            startActivity(intent)
        }
        else{
            val builder = AlertDialog.Builder(activity)
            builder.setTitle("Sin Internet")
            builder.setMessage("Para acceder a esta función necesitas internet, por favor revisa tu conexión y intenta más tarde.")

            val dialog = builder.create()
            dialog.show()
        }
    }

}
