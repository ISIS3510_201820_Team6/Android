package mos.mobile.uniandes.gymar.models.trainer

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import mos.mobile.uniandes.gymar.entities.ExerciseEntity
import mos.mobile.uniandes.gymar.entities.UserEntity
import mos.mobile.uniandes.gymar.helpers.FireBaseDBConstants
import mos.mobile.uniandes.gymar.models.GymDataBase
import mos.mobile.uniandes.gymar.models.exercises.ExercisesByCategoryRepository
import mos.mobile.uniandes.gymar.pojos.TrainerUser

class TrainerRepository( ) {

//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------

    private val TAG: String = TrainerRepository::class.java.simpleName

    private val gymFireStore: FirebaseFirestore

    private lateinit var query: Query

    private var mAuth: FirebaseAuth

    init {
        gymFireStore = FirebaseFirestore.getInstance()
        mAuth = FirebaseAuth.getInstance()
    }

//------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------

    private var clients: MutableLiveData<MutableList<TrainerUser>> = MutableLiveData()


    private lateinit var listenerFirestore: ListenerRegistration

//------------------------------------------------------------------------
// PUBLIC FUNCITONS
//------------------------------------------------------------------------

    fun getClientsLookingForHelp(conectivity: Boolean): LiveData<MutableList<TrainerUser>> {
        Log.d(TAG, "Getting Clients from repository")
        Log.d(TAG, "Conectivity? $conectivity")
        if (clients.value == null){
            clients.postValue(mutableListOf<TrainerUser>())
            if (conectivity){
                Log.d(TAG, "Fetching Remotely")
                fetchActiveClientsOfTrainer( )
            }
        }
        return clients
    }

    fun updateClient(connectivity: Boolean, trainerUser: TrainerUser){
        if(clients.value?.remove(trainerUser)!!){
            clients.postValue(clients.value)
            Log.d(TAG, "client removed! Clients: ${clients.value?.size}")
        }
        gymFireStore.collection(FireBaseDBConstants.TRAINER_USER_TABLE)
            .document(trainerUser.id).update(FireBaseDBConstants.TRAINER_USER_REALIZED, true)
            .addOnSuccessListener() {
                Log.d(TAG, "Document was Succesfully updated")
            }.addOnFailureListener(){
                Log.e(TAG, "Document was nos updated")
            }
    }

    fun detachPosibleListeners(){
        if(::listenerFirestore.isInitialized){
            listenerFirestore.remove()
        }
    }

//------------------------------------------------------------------------
// PRIVATE FUNCITONS
//------------------------------------------------------------------------

    private fun fetchActiveClientsOfTrainer( ){
        var currentUser = mAuth.currentUser
        if (currentUser != null){
            defineQuery(currentUser.uid)
            query.addSnapshotListener(EventListener<QuerySnapshot>{ querySnapshot, exception ->
                Log.d(TAG, "ACTIVATED!!")
                if(exception != null){
                    Log.e(TAG, "Error en el SnapshotListener!!")
                    return@EventListener
                }
                else{
                    Log.d(TAG, "Fetching Elements! Clients: ${clients.value?.size}")
                    querySnapshot?.documentChanges?.forEach{document ->
                        when(document.type){
                            DocumentChange.Type.ADDED ->{
                                clients.value?.add(FireBaseDBConstants.createTrainerUser(document.document))
                                Log.d(TAG, "# ${clients.value?.size}")
                                clients.postValue(clients.value)
                            }
                            DocumentChange.Type.MODIFIED ->{
                                clients.value?.add(FireBaseDBConstants.createTrainerUser(document.document))
                                Log.d(TAG, "# ${clients.value?.size}")
                                clients.postValue(clients.value)
                            }
                            DocumentChange.Type.REMOVED ->{
                                Log.d(TAG, "Removed!")
                            }
                        }
                    }
                }
            })
        }
    }

    private fun defineQuery(userId: String){
        if(!::query.isInitialized){
            query = gymFireStore.collection(FireBaseDBConstants.TRAINER_USER_TABLE)
                    .whereEqualTo(FireBaseDBConstants.TRAINER_USER_TRAINER, userId)
                    .whereEqualTo(FireBaseDBConstants.TRAINER_USER_REALIZED, false)
        }
    }

}