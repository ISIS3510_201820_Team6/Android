package mos.mobile.uniandes.gymar.models.statistics

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.graphics.Color
import android.util.Log
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import mos.mobile.uniandes.gymar.helpers.FireBaseDBConstants
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class StatisticsRepository() {

//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------

    private val TAG: String = StatisticsRepository::class.java.simpleName

    private val queryLimit: Long = 12

    private val gymFireStore: FirebaseFirestore

    private var mAuth: FirebaseAuth

    init {
        gymFireStore = FirebaseFirestore.getInstance()
        mAuth = FirebaseAuth.getInstance()
    }

//------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------

    private var fatData: MutableLiveData<LineDataSet?> = MutableLiveData()

    private var weightData: MutableLiveData<LineDataSet?> = MutableLiveData()

    private var perimeterData: MutableLiveData<LineDataSet?> = MutableLiveData()

    private lateinit var listenerFirestore: ListenerRegistration

//------------------------------------------------------------------------
// PUBLIC FUNCITONS
//------------------------------------------------------------------------

    fun configureStatisticData(connectivity: Boolean){
        if (connectivity){
            fetchStatisticsData()
        }
    }

    fun getFatStatisticsData( ): LiveData<LineDataSet?>{
        return fatData
    }

    fun getWeightStatisticsData( ): LiveData<LineDataSet?>{
        return weightData
    }

    fun getPerimeterStatisticsData( ): LiveData<LineDataSet?>{
        return perimeterData
    }

    fun detachPosibleListeners(){
        if(::listenerFirestore.isInitialized){
            listenerFirestore.remove()
        }
    }

//------------------------------------------------------------------------
// PRIVATE FUNCITONS
//------------------------------------------------------------------------

    private fun fetchStatisticsData( ){
        val currentUser = mAuth.currentUser
        if (currentUser != null){
            val query: Query = gymFireStore.collection(FireBaseDBConstants.STATISTICS_TABLE)
                .whereEqualTo(FireBaseDBConstants.STATISTICS_USER, currentUser.uid)
                .orderBy(FireBaseDBConstants.STATISTICS_DATE, Query.Direction.DESCENDING)
                .limit(queryLimit)

            listenerFirestore = query.addSnapshotListener(EventListener<QuerySnapshot>{ querySnapshot, exception ->
                if(exception != null){
                    Log.e(TAG, "Error en el SnapshotListener!!")
                    Log.e(TAG, exception.message)
                    return@EventListener
                }
                else{
                    doAsync {

                        val fatEntries: ArrayList<Entry> = ArrayList()
                        val weightEntries: ArrayList<Entry> = ArrayList()
                        val perimeterEntries: ArrayList<Entry> = ArrayList()

                        var i: Float = 1F
                        if (i != null) {
                            if(i > 0) {
                                querySnapshot?.forEach{ document ->
                                    Log.d(TAG, "Document: $document")
                                    fatEntries.add(Entry(i, (document.get(FireBaseDBConstants.STATISTICS_FAT) as Long).toFloat()))
                                    weightEntries.add(Entry(i, (document.get(FireBaseDBConstants.STATISTICS_WEIGHT) as Long).toFloat()))
                                    perimeterEntries.add(Entry(i, (document.get(FireBaseDBConstants.STATISTICS_PERIMETER) as Long).toFloat()))
                                    i++
                                }

                                val fatDataSet = LineDataSet(fatEntries, "%graso")
                                val weightDataSet = LineDataSet(weightEntries, "peso [kg]")
                                val perimeterDataSet = LineDataSet(perimeterEntries, "perimetro [cm]")

                                fatDataSet.fillAlpha = 65
                                fatDataSet.color = Color.rgb(234,73,24)
                                fatDataSet.lineWidth = 3F
                                fatDataSet.valueTextSize = 10F

                                weightDataSet.fillAlpha = 65
                                weightDataSet.color = Color.rgb(234,73,24)
                                weightDataSet.lineWidth = 3F
                                weightDataSet.valueTextSize = 10F

                                perimeterDataSet.fillAlpha = 65
                                perimeterDataSet.color = Color.rgb(234,73,24)
                                perimeterDataSet.lineWidth = 3F
                                perimeterDataSet.valueTextSize = 10F

                                uiThread {
                                    Log.d(TAG, "Posting values")
                                    fatData.postValue(fatDataSet)
                                    weightData.postValue(weightDataSet)
                                    perimeterData.postValue(perimeterDataSet)
                                }
                            }
                        }
                    }
                }
            })
        }
    }
}