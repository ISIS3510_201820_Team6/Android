package mos.mobile.uniandes.gymar.models.exercises

import android.arch.lifecycle.LiveData
import mos.mobile.uniandes.gymar.entities.ExerciseEntity

interface ExerciseRepository<T> {

    fun getExercises(conectivity: Boolean, searchParam: String): LiveData<MutableList<T>>

    fun detachPosibleListeners()
}