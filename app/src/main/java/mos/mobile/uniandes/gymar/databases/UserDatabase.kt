package mos.mobile.uniandes.gymar.databases

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import mos.mobile.uniandes.gymar.daos.UserDao
import mos.mobile.uniandes.gymar.entities.UserEntity

@Database(entities = [(UserEntity::class)], version = 1, exportSchema = false)
abstract class UserDatabase: RoomDatabase(){
    abstract fun userDao(): UserDao

}