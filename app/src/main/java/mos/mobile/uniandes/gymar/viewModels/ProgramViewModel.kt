package mos.mobile.uniandes.gymar.viewModels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.util.Log
import mos.mobile.uniandes.gymar.entities.PlanEntity
import mos.mobile.uniandes.gymar.entities.RoutineEntity
import mos.mobile.uniandes.gymar.models.routines.PlanRepository

class ProgramViewModel: ViewModel() {

//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------
    private val TAG: String = ProgramViewModel::class.java.simpleName

//------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------

    private lateinit var routines: LiveData<List<RoutineEntity>>

    private lateinit var plans: LiveData<List<PlanEntity?>>

    private lateinit var routineRepository: PlanRepository

//------------------------------------------------------------------------
// PUBLIC FUNCITONS
//------------------------------------------------------------------------

    /**
     * Returns a liveData list with the routines according to the specified parameters
     */
    fun getRoutines(context: Context): LiveData<List<RoutineEntity>> {
        Log.d(TAG, "Getting Exercises")
        if(!::routines.isInitialized){
            Log.d(TAG, "Fetching exercises")
            routines = loadRoutines(context)
        }
        return routines
    }

    /**
     * Returns a liveData list with the exercises according to the specified parameters
     */
    fun getPlan(context: Context): LiveData<List<PlanEntity?>>{
        Log.d(TAG, "Getting Exercises")
        if(!::routines.isInitialized){
            Log.d(TAG, "Fetching exercises")
            plans = loadPlan(context)
        }
        return plans
    }

    /**
     * Detaches the listeners that might have been initialized
     */
    fun detachListeners(){
        if(routineRepository != null){
            routineRepository.detachPosibleListeners()
        }
    }

//------------------------------------------------------------------------
// PRIVATE FUNCITONS
//------------------------------------------------------------------------

    /**
     * Returns a LiveData list with the exercises depending on the connectivity of the device
     */
    private fun loadRoutines(context: Context ): LiveData<List<RoutineEntity>>  {
        routineRepository = PlanRepository (context)
        return routineRepository.getRoutines( )
    }

    /**
     * Returns a LiveData list with the exercises depending on the connectivity of the device
     */
    private fun loadPlan(context: Context ): LiveData<List<PlanEntity?>> {
        routineRepository = PlanRepository (context)
        return routineRepository.getPlan( )
    }
}