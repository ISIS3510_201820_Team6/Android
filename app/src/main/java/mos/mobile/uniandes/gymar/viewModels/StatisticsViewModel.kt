package mos.mobile.uniandes.gymar.viewModels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.util.Log
import com.github.mikephil.charting.data.LineDataSet
import mos.mobile.uniandes.gymar.helpers.ConectivityHelper
import mos.mobile.uniandes.gymar.models.statistics.StatisticsRepository

class StatisticsViewModel: ViewModel() {

//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------

    private val TAG: String = StatisticsViewModel::class.java.simpleName

    private val statisticsRepository: StatisticsRepository = StatisticsRepository()

//------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------

    private lateinit var fatData: LiveData<LineDataSet?>

    private lateinit var weightData: LiveData<LineDataSet?>

    private lateinit var perimeterData: LiveData<LineDataSet?>

//------------------------------------------------------------------------
// PUBLIC FUNCITONS
//------------------------------------------------------------------------

    fun configureData(context: Context){
        Log.d(TAG, "Configuration of LiveDatas")
        statisticsRepository.configureStatisticData(ConectivityHelper.checkOverallConectivity(context))
    }

    /**
     * Returns a liveData list with the exercises according to the specified parameters
     */
    fun getFatStatistics(context: Context): LiveData<LineDataSet?> {
        Log.d(TAG, "Getting Data")
        if (!::fatData.isInitialized) {
            Log.d(TAG, "Fetching Data")
            fatData = getFatLineDataSet(context)
        }
        return fatData
    }

    /**
     * Returns a liveData list with the Data according to the specified parameters
     */
    fun getWeightStatistics(context: Context): LiveData<LineDataSet?> {
        Log.d(TAG, "Getting Data")
        if (!::weightData.isInitialized) {
            Log.d(TAG, "Fetching Data")
            weightData = getWeightLineDataSet(context)
        }
        return weightData
    }

    /**
     * Returns a liveData list with the Data according to the specified parameters
     */
    fun getPerimeterStatistics(context: Context): LiveData<LineDataSet?> {
        Log.d(TAG, "Getting Data")
        if (!::perimeterData.isInitialized) {
            Log.d(TAG, "Fetching Data")
            perimeterData = getPerimeterLineDataSet(context)
        }
        return perimeterData
    }

    /**
     * Detaches the listeners that might have been initialized
     */
    fun detachListeners(){
        statisticsRepository.detachPosibleListeners()
    }

//------------------------------------------------------------------------
// PRIVATE FUNCITONS
//------------------------------------------------------------------------

    private fun getFatLineDataSet(context: Context): LiveData<LineDataSet?>{
        return statisticsRepository.getFatStatisticsData()
    }

    private fun getWeightLineDataSet(context: Context): LiveData<LineDataSet?>{
        return statisticsRepository.getWeightStatisticsData()
    }

    private fun getPerimeterLineDataSet(context: Context): LiveData<LineDataSet?>{
        return statisticsRepository.getPerimeterStatisticsData()
    }
}