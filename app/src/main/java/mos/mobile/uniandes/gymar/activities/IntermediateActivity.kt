package mos.mobile.uniandes.gymar.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.gymar.vuf.UnityPlayerActivity
import mos.mobile.uniandes.gymar.R

class IntermediateActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val intent = Intent(this, UnityPlayerActivity::class.java)
        startActivity(intent)
    }
}
