package mos.mobile.uniandes.gymar.viewModels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.util.Log
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import mos.mobile.uniandes.gymar.helpers.ConectivityHelper
import mos.mobile.uniandes.gymar.models.statistics.MyBodyRepository

class MyBodyViewModel: ViewModel(){

//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------
    private val TAG: String = MyBodyViewModel::class.java.simpleName

//------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------
    private lateinit var data: LiveData<BarDataSet?>

    private lateinit var myBodyRepository: MyBodyRepository

//------------------------------------------------------------------------
// PUBLIC FUNCITONS
//------------------------------------------------------------------------

    /**
     * Returns a liveData list with the exercises according to the specified parameters
     */
    fun getClients(context: Context): LiveData<BarDataSet?> {
        Log.d(TAG, "Getting Exercises")
        if (!::data.isInitialized) {
            Log.d(TAG, "Fetching exercises")
            data = getClientsSeekingHelp(context)
        }
        return data
    }

    /**
     * Detaches the listeners that might have been initialized
     */
    fun detachListeners(){
        myBodyRepository.detachPosibleListeners()
    }

//------------------------------------------------------------------------
// PRIVATE FUNCITONS
//------------------------------------------------------------------------

    private fun getClientsSeekingHelp(context: Context): LiveData<BarDataSet?> {
        myBodyRepository = MyBodyRepository()
        return myBodyRepository.getMyBodyData(ConectivityHelper.checkOverallConectivity(context))
    }
}