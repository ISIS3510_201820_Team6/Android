package mos.mobile.uniandes.gymar.viewModels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.util.Log
import mos.mobile.uniandes.gymar.entities.ExerciseEntity
import mos.mobile.uniandes.gymar.helpers.ConectivityHelper
import mos.mobile.uniandes.gymar.helpers.FireBaseDBConstants
import mos.mobile.uniandes.gymar.models.exercises.ExerciseRepository
import mos.mobile.uniandes.gymar.models.exercises.ExercisesByCategoryRepository
import mos.mobile.uniandes.gymar.models.exercises.ExercisesByRoutineRepository

class ExercisesByCategoryViewModel: ViewModel() {


//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------
    private val TAG: String = ExercisesByCategoryViewModel::class.java.simpleName

//------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------
    private lateinit var exercises: LiveData<MutableList<ExerciseEntity>>

    private lateinit var exerciseRepository: ExerciseRepository<ExerciseEntity>
//------------------------------------------------------------------------
// PUBLIC FUNCITONS
//------------------------------------------------------------------------

    /**
     * Returns a liveData list with the exercises according to the specified parameters
     */
    fun getExercises(context: Context, searchParam: String): LiveData<MutableList<ExerciseEntity>> {
        Log.d(TAG, "Getting Exercises")
        if(!::exercises.isInitialized){
            Log.d(TAG, "Fetching exercises")
            exercises = loadExercises(context, searchParam)
        }
        return exercises
    }

    /**
     * Detaches the listeners that might have been initialized
     */
    fun detachListeners(){
        exerciseRepository.detachPosibleListeners()
    }

//------------------------------------------------------------------------
// PRIVATE FUNCITONS
//------------------------------------------------------------------------

    /**
     * Returns a LiveData list with the exercises depending on the connectivity of the device
     */
    private fun loadExercises(context: Context,  searchParam: String): LiveData<MutableList<ExerciseEntity>> {
        var connectivity = ConectivityHelper.checkOverallConectivity(context)
        exerciseRepository = ExercisesByCategoryRepository(context)
        return exerciseRepository.getExercises(connectivity, searchParam)
    }
}