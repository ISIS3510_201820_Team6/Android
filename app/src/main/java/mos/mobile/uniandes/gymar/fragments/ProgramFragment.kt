package mos.mobile.uniandes.gymar.fragments

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.LoaderManager
import android.support.v4.content.Loader
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import mos.mobile.uniandes.gymar.R
import mos.mobile.uniandes.gymar.activities.plans.PlanAsyncTaskLoader
import mos.mobile.uniandes.gymar.activities.plans.RoutinesAdapter
import mos.mobile.uniandes.gymar.models.GymDataBase
import mos.mobile.uniandes.gymar.entities.ExerciseEntity
import mos.mobile.uniandes.gymar.entities.PlanEntity
import mos.mobile.uniandes.gymar.entities.RoutineEntity
import mos.mobile.uniandes.gymar.entities.RoutineExerciseEntity
import mos.mobile.uniandes.gymar.helpers.ConectivityHelper
import mos.mobile.uniandes.gymar.helpers.FireBaseDBConstants
import mos.mobile.uniandes.gymar.viewModels.ProgramViewModel
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class ProgramFragment : Fragment(), LoaderManager.LoaderCallbacks<PlanEntity> {

//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------
    /**
     * TAG para el uso de Logs en la aplicacion
     */
    private val TAG = ProgramFragment::class.java.simpleName

    companion object {
        private const val OPERATION_SEARCH_LOADER = 123
    }

//------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------

    /**
     *  Linear layout manager
     */
    private lateinit var gridLayoutManager: GridLayoutManager

    /**
     * recyclerView
     */
    private lateinit var recyclerView: RecyclerView

    /**
     * Adapter for the recyclerManager
     */
    private lateinit var adapter: RoutinesAdapter

    /**
     *
     */
    private lateinit var nameTextView: TextView

    /**
     *
     */
    private lateinit var descriptionTextView: TextView

    /**
     *
     */
    private lateinit var progressBar: ProgressBar

    /**
     *
     */
    private lateinit var viewModel: ProgramViewModel

//------------------------------------------------------------------------
// LIFECYLE'S FUNCTIONS
//------------------------------------------------------------------------

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View? = inflater.inflate(R.layout.fragment_program, container, false)

        recyclerView = view?.findViewById(R.id.activity_plan_recyclerview) as RecyclerView
        gridLayoutManager = GridLayoutManager(view.context, 2)
        recyclerView.layoutManager = gridLayoutManager

        adapter = RoutinesAdapter(arrayListOf<RoutineEntity>(), activity as Activity)
        recyclerView.adapter = adapter

        nameTextView = view.findViewById(R.id.fragment_program_name)
        descriptionTextView = view.findViewById(R.id.fragment_program_description)
        progressBar = view.findViewById(R.id.fragment_program_progressbar)

        if(loaderManager.getLoader<PlanAsyncTaskLoader>(OPERATION_SEARCH_LOADER) == null){
            Log.d(TAG, "Creacion de Loader")
            loaderManager.initLoader(OPERATION_SEARCH_LOADER, null, this)
        }

        viewModel = ViewModelProviders.of(this).get(ProgramViewModel::class.java)

        return view
    }

    override fun onStart() {
        super.onStart()
        viewModel.getPlan(activity!!.applicationContext).observe(this, Observer { plans ->
            if(plans != null && plans.isNotEmpty()){
                assingLayoutData(plans[0]!!)
            }
            else{
                Log.e(TAG, "Error while updating Plan LiveData")
            }
        })

        viewModel.getRoutines(activity!!.applicationContext).observe(this, Observer { routines ->
            if(routines != null){
                adapter.changeData(routines)
            }
            else{
                Log.e(TAG, "Error while updating Routine LiveData List")
            }
        })
    }

    override fun onResume() {
        super.onResume()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    override fun onPause() {
        super.onPause()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
    }

    override fun onStop() {
        super.onStop()
        viewModel.detachListeners()
    }

//------------------------------------------------------------------------
// SETUP FUNCTIONS
//------------------------------------------------------------------------

    override fun onCreateLoader(p0: Int, p1: Bundle?): Loader<PlanEntity> {
        Log.d(TAG, "OnCreate Loader!!!")
        return PlanAsyncTaskLoader(activity?.applicationContext!!)
    }

    override fun onLoadFinished(p0: Loader<PlanEntity>, p1: PlanEntity?) {
        if(p1 != null){

        }
    }

    override fun onLoaderReset(p0: Loader<PlanEntity>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }



    /**
     *
     */
    fun assingLayoutData(planEntity: PlanEntity){
        nameTextView.text = planEntity.objetivo
        descriptionTextView.text = "Nivel: ${planEntity.nivel}"
        progressBar.progress = 65
    }


}
