package mos.mobile.uniandes.gymar.daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import mos.mobile.uniandes.gymar.entities.PlanEntity

@Dao
interface PlanDao: CommonDao<PlanEntity> {

    @Query(value = "SELECT * FROM planes")
    fun getAllPlanEntities():LiveData<List<PlanEntity?>>

    @Query(value = "SELECT * FROM planes WHERE id = :planId")
    fun getPlansById(planId: String): PlanEntity?

    @Query(value = "SELECT * FROM planes")
    fun getAllPlansEntities(): List<PlanEntity>?


    @Query(value = "DELETE FROM planes")
    fun deleteAll()

}