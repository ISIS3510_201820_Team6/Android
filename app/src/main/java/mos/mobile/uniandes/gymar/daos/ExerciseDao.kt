package mos.mobile.uniandes.gymar.daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import mos.mobile.uniandes.gymar.entities.ExerciseEntity

@Dao
interface ExerciseDao: CommonDao<ExerciseEntity> {

    @Query(value = "SELECT * FROM ejercicios")
    fun getAllExerciseEntities(): LiveData<List<ExerciseEntity>>

    @Query(value = "SELECT * FROM ejercicios WHERE categoria = :category")
    fun getAllExerciseEntitiesByCategory(category: String): LiveData<MutableList<ExerciseEntity>>

    @Query(value = "SELECT * FROM ejercicios WHERE id = :exerciseId")
    fun getExerciseById(exerciseId: String): ExerciseEntity

    @Query(value = "DELETE FROM ejercicios")
    fun deleteAll()

    @Query(value = "SELECT * FROM ejercicios INNER JOIN rutina_ejercicio ON ejercicios.dosId = rutina_ejercicio.exerciseId WHERE rutina_ejercicio.routineId = :routineId")
    fun getAllExerciseEntitiesByRoutineId(routineId: String): List<ExerciseEntity>

    @Transaction
    fun insertExercises(eList: ArrayList<ExerciseEntity>){
        eList.forEach { e ->
            insert(e)
        }
    }
}