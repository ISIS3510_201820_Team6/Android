package mos.mobile.uniandes.gymar.activities.plans

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import mos.mobile.uniandes.gymar.fragments.ProgramFragment
import mos.mobile.uniandes.gymar.R
import mos.mobile.uniandes.gymar.activities.exercises.ExerciseListByRoutineActivity
import mos.mobile.uniandes.gymar.entities.RoutineEntity
import org.jetbrains.anko.startActivity

class RoutinesAdapter(private val routines: MutableList<RoutineEntity>, private val activity: Activity): RecyclerView.Adapter<RoutinesAdapter.ViewHolder>(){

    fun changeData(routineList: List<RoutineEntity>){
        routines.clear()
        routines.addAll(routineList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): RoutinesAdapter.ViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.fragment_program_recycleview_cardview, viewGroup, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return routines.size
    }

    override fun onBindViewHolder(viewHolder: RoutinesAdapter.ViewHolder, position: Int) {
        var routineEntity = routines[position]

        viewHolder.bindRoutineLayoutData(routineEntity)

        viewHolder.itemView.setOnClickListener{
            activity.startActivity<ExerciseListByRoutineActivity>(
                    "EXERCISE_ROUTINE" to routineEntity.id, "ROUTINE_NAME" to routineEntity.nombre)
            Log.d(ProgramFragment::class.java.simpleName, "Element Clicked")
        }
    }

    /**
     * View Holder para el RoutinesAdapter
     */
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        /**
         * TextView of the Row Item Layout that contains the nombre of the exercise
         */
        private val routineTitle: TextView = itemView.findViewById<TextView>(R.id.activity_plan_cardview_title)

        /**
         * TextView of the Row Item Layout that contains the description of the exercise
         */
        private val routineDescripiton: TextView = itemView.findViewById<TextView>(R.id.activity_plan_cardview_description)

        /**
         *
         */
        private val routineImage: ImageView = itemView.findViewById<ImageView>(R.id.activity_plan_cardview_image)


        fun bindRoutineLayoutData(routineEntity: RoutineEntity){
            routineTitle.text = routineEntity.nombre
            routineDescripiton.text = routineEntity.tipo

            val imageContext = routineImage.context
            val iconIdentifier = imageContext.resources.getIdentifier(routineEntity.imagen,"drawable", imageContext.packageName)
            routineImage.setImageResource(iconIdentifier)

        }
    }

}