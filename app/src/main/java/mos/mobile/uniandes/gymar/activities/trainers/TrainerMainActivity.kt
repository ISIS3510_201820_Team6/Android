package mos.mobile.uniandes.gymar.activities.trainers

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.google.firebase.auth.FirebaseAuth
import mos.mobile.uniandes.gymar.LoginActivity
import mos.mobile.uniandes.gymar.MainActivity
import mos.mobile.uniandes.gymar.R
import mos.mobile.uniandes.gymar.activities.exercises.ExercisesAdapter
import mos.mobile.uniandes.gymar.pojos.TrainerUser
import mos.mobile.uniandes.gymar.viewModels.TrainerViewModel
import org.jetbrains.anko.alert

class TrainerMainActivity : AppCompatActivity() {

    /**
     * Firebase Auth
     */
    private lateinit var mAuth: FirebaseAuth

//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------

    /**
    * TAG para el uso de Logs en la aplicacion
    */
    private val TAG = TrainerMainActivity::class.java.simpleName

//------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------

    /**
     *  Linear layout manager
     */
    private lateinit var linearLayoutManager: LinearLayoutManager

    /**
     * recyclerView
     */
    private lateinit var recyclerView: RecyclerView

    /**
     * Adapter for the recyclerManager
     */
    private lateinit var adapter: TrainerListAdapter

    /**
     * ViewModel for the activity
     */
    private lateinit var viewModel: TrainerViewModel

//------------------------------------------------------------------------
// LIFECYCLE FUNTIONS
//------------------------------------------------------------------------

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trainer_main)

        mAuth = FirebaseAuth.getInstance()

        var toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        //Defines Elements of the Activity/View
        recyclerView = findViewById(R.id.activity_trainer_recyclerview)
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        adapter = TrainerListAdapter(mutableListOf<TrainerUser>(), this)
        recyclerView.adapter = adapter

        viewModel = ViewModelProviders.of(this).get(TrainerViewModel::class.java)

        //Handler for swiping effect
        val swipeHandler = object : SwipeRemover(this){
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val vAdapter = recyclerView.adapter as TrainerListAdapter
                var tu = adapter.removeAt(viewHolder.adapterPosition)
                viewModel.updateClientStatus(this@TrainerMainActivity.applicationContext, tu)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "Starting Activity!!")
        viewModel.getClients(this.applicationContext).observe(this, Observer { clients ->
            if (clients != null && clients.isNotEmpty()){
                Log.d(TAG, "Clients Found!")
                adapter.setClients(clients)
            }
            else{
                Log.e(TAG, "Error while updating Exercises LiveData List")
            }
        })
    }

    override fun onStop() {
        super.onStop()
        viewModel.detachListeners()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.logout_option -> {
            // User chose the "Settings" item, show the app settings UI...
            alert("¿Está seguro de que desea cerrar sesión?") {
                title = "Cerrar Sesión"
                positiveButton("Sí") { logout() }
                negativeButton("Cancelar") { }
            }.show().apply {
                getButton(AlertDialog.BUTTON_POSITIVE)?.let { it.setTextColor(Color.BLACK) }
                getButton(AlertDialog.BUTTON_NEGATIVE)?.let { it.setTextColor(Color.BLACK) }
            }

            true
        }

        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

    fun logout(){
        mAuth.signOut()
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

//------------------------------------------------------------------------
// SETUP FUNCTIONS
//------------------------------------------------------------------------

}
