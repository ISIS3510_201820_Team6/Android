package mos.mobile.uniandes.gymar.entities

import android.arch.persistence.room.*
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

//Entity that describes the Routine DB scheme
@Parcelize
@Entity(tableName = "rutinas",
        foreignKeys = [ForeignKey(
                entity = PlanEntity::class,
                parentColumns = ["id"],
                childColumns = ["planId"],
                onDelete = ForeignKey.CASCADE
        )],
        indices = [Index(value = "planId", name = "planesi")])
data class RoutineEntity(
        @PrimaryKey @ColumnInfo(name = "id") var id: String,
        @ColumnInfo(name = "nombre") var nombre: String,
        @ColumnInfo(name = "tipo") var tipo: String,
        @ColumnInfo(name = "imagen") var imagen: String,
        @ColumnInfo(name = "planId") var planId: String): Parcelable{

    @Ignore
    constructor():this("", "","","", "")
}
