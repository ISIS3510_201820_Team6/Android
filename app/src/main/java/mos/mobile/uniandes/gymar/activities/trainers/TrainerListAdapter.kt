package mos.mobile.uniandes.gymar.activities.trainers

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import mos.mobile.uniandes.gymar.R
import mos.mobile.uniandes.gymar.pojos.TrainerUser
import org.jetbrains.anko.startActivity

class TrainerListAdapter(private val clients: MutableList<TrainerUser>, private val activity: Activity): RecyclerView.Adapter<TrainerListAdapter.ViewHolder>(){

    private val TAG = TrainerListAdapter::class.java.simpleName

    fun setClients(clientList: MutableList<TrainerUser>){
        clients.clear()
        clients.addAll(clientList)
        notifyDataSetChanged()
    }

    fun removeAt(position: Int): TrainerUser{
        var tu: TrainerUser = clients.removeAt(position)
        notifyItemRemoved(position)
        return tu
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): TrainerListAdapter.ViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.activity_trainer_main_item, viewGroup, false)
        return ViewHolder(view, activity)
    }

    override fun getItemCount(): Int {
        return clients.size
    }

    override fun onBindViewHolder(viewHolder: TrainerListAdapter.ViewHolder, position: Int)  {
        val trainerUser: TrainerUser = clients[position]

        //data binding with ViewHolder
        viewHolder.bindTrainerUserLayoutData(trainerUser)

        viewHolder.itemView.setOnClickListener{
            activity.startActivity<UserDetailActivity>("USER_NAME" to trainerUser.cliente,
                    "USER_IMAGE" to trainerUser.imagen, "USER_EXERCISE" to trainerUser.ejercicio,
                    "USER_CONDITIONS" to trainerUser.condiciones, "USER_ID" to trainerUser.cId,
                    "TRAINER_NAME" to trainerUser.nombreEntrenador)
        }
    }


    class ViewHolder(itemView: View, activity: Activity): RecyclerView.ViewHolder(itemView){

        /**
         * TextView of the Row Item Layout that contains the name of the exercise
         */
        private val exerciseNameText: TextView = itemView.findViewById<TextView>(R.id.activity_trainers_recyclerview_nameText)

        /**
         * TextView of the Row Item Layout that contains the description of the exercise
         */
        private val exerciseDescriptionText: TextView = itemView.findViewById<TextView>(R.id.activity_trainers_recyclerview_descriptionText)

        /**
         *
         */
        private val exercisesIcon: ImageView = itemView.findViewById<ImageView>(R.id.activity_trainers_recyclerview_icon)

        /**
         *
         */
        val options = RequestOptions().error(R.drawable.network_error)

        /**
         *
         */
        val glideRequestManager: RequestManager = Glide.with(activity)

        fun bindTrainerUserLayoutData(trainerUser: TrainerUser){
            exerciseNameText.text = trainerUser.cliente
            exerciseDescriptionText.text = "Ubicado en ${trainerUser.ejercicio}"

            glideRequestManager.load(trainerUser.imagen).thumbnail(glideRequestManager.load(R.drawable.loading_icon)).apply(options).into(exercisesIcon)

        }
    }
}