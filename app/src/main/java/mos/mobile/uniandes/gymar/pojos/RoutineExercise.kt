package mos.mobile.uniandes.gymar.pojos

import com.google.firebase.firestore.DocumentReference
import mos.mobile.uniandes.gymar.entities.ExerciseEntity

data class RoutineExercise(
        var numeroRepeticionesPorSerie: Long,
        var numeroSeries: Long,
        var ejercicio: ExerciseEntity)