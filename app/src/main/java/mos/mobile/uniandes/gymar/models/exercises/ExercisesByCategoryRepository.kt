package mos.mobile.uniandes.gymar.models.exercises

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.util.Log
import com.google.firebase.firestore.*
import mos.mobile.uniandes.gymar.entities.ExerciseEntity
import mos.mobile.uniandes.gymar.helpers.FireBaseDBConstants
import mos.mobile.uniandes.gymar.models.GymDataBase
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception
import kotlin.coroutines.experimental.coroutineContext

class ExercisesByCategoryRepository(context: Context): ExerciseRepository<ExerciseEntity> {

//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------

    private val TAG: String = ExercisesByCategoryRepository::class.java.simpleName

    private val gymFireStore: FirebaseFirestore

    private val gymDB: GymDataBase

    init {
        gymDB = GymDataBase.getInstance(context)!!
        gymFireStore = FirebaseFirestore.getInstance()
    }

//------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------

    private var exercises: MutableLiveData<MutableList<ExerciseEntity>> = MutableLiveData()

    private lateinit var exercisesLive: LiveData<MutableList<ExerciseEntity>>

    private lateinit var listenerFirestore: ListenerRegistration

//------------------------------------------------------------------------
// PUBLIC FUNCTIONS
//------------------------------------------------------------------------

    /**
     * Gets the exercises according to a specific category
     */
    override fun getExercises(conectivity: Boolean, searchParam: String): LiveData<MutableList<ExerciseEntity>> {
        Log.d(TAG, "Getting Exercises from repository")
        Log.d(TAG, "Conectivity? $conectivity")
        if (!conectivity){
            if(!::exercisesLive.isInitialized) {
                Log.d(TAG, "Fetching Locally")
                loadExcersisesFromStorage(searchParam)
            }
            return exercisesLive
        }
        else {
            if(exercises.value == null){
                Log.d(TAG, "Fetching Remotely")
                fetchExercisesByCategory(searchParam)
            }
            return exercises
        }
    }

    /**
     * Removes the listener when it is no longer needed
     */
    override fun detachPosibleListeners(){
        if(::listenerFirestore.isInitialized){
            listenerFirestore.remove()
        }
    }

//------------------------------------------------------------------------
// PRIVATE FUNCITONS
//------------------------------------------------------------------------

    /**
     * Returns a LiveData list with the exercises from Room, depending on the search type (cateogry or routine)
     */
    private fun loadExcersisesFromStorage(searchParam: String){
        exercisesLive = gymDB.exerciseDao().getAllExerciseEntitiesByCategory(searchParam)
    }

    /**
     * Fetches Exercises from Firestore that belong to the category of the parameter
     */
    private fun fetchExercisesByCategory(searchParam: String){
        var query = defineQueryforCategoryFetch(searchParam)


        listenerFirestore = query.addSnapshotListener(EventListener<QuerySnapshot>{querySnapshot, exception ->

            if(exception != null){
                Log.d(TAG, "Error en el SnapshotListener!!")
                return@EventListener
            }
            else{
                doAsync {
                    var exerciseList: ArrayList<ExerciseEntity> = ArrayList<ExerciseEntity>()
                    querySnapshot?.forEach { document ->
                        exerciseList.add(FireBaseDBConstants.createExerciseEntity(document))
                    }

                    uiThread {
                        exercises.postValue(exerciseList)
                    }
                }
            }
        })

    }

    /**
     * Defines the queries used in the Activity
     */
    private fun defineQueryforCategoryFetch(searchParam: String): Query{
        val dbExercises = gymFireStore.collection(FireBaseDBConstants.EXERCISES_TABLE)

        var queryCategory = FirebaseFirestore.getInstance().collection(FireBaseDBConstants.CATEGORIES_TABLE).document(searchParam)
        return dbExercises.whereEqualTo(FireBaseDBConstants.EXERCISE_CATEGORY,queryCategory).orderBy(FireBaseDBConstants.EXERCISES_NAME, Query.Direction.ASCENDING)

    }



}

