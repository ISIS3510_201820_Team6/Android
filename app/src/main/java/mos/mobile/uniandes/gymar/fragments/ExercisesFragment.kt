package mos.mobile.uniandes.gymar.fragments

import android.app.Activity
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import mos.mobile.uniandes.gymar.R
import mos.mobile.uniandes.gymar.activities.exercises.ExerciseCategoriesAdapter
import mos.mobile.uniandes.gymar.models.GymDataBase
import mos.mobile.uniandes.gymar.entities.ExerciseCategoryEntity
import mos.mobile.uniandes.gymar.helpers.ConectivityHelper
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception


class ExercisesFragment : Fragment() {

//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------
    /**
     * TAG para el uso de Logs en la aplicacion
     */
    private val TAG = ExercisesFragment::class.java.simpleName

    /**
     * DB instance for queries
     */
    private val db = FirebaseFirestore.getInstance().collection(CATEGORIES_TABLE)

    /**
     * Initial query to collect the exercises
     */
    private lateinit var query: Query

    companion object {
        private const val CATEGORIES_TABLE = "categorias"
        private const val CATEGORIES_NAME = "nombre"
        private const val CATEGORIES_ICON_ANDRIOD = "iconoAndroid"
        private const val CATEGORIES_ICON_IOS = "iconoIOS"
    }

//------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------

    /**
     *
     */
    private lateinit var dbGym: GymDataBase

    /**
     * List of exercise categories
     */
    private lateinit var categories: ArrayList<ExerciseCategoryEntity>

    /**
     *  Linear layout manager
     */
    private lateinit var linearLayoutManager: LinearLayoutManager

    /**
     * recyclerView
     */
    private lateinit var recyclerView: RecyclerView

    /**
    * Adapter for the recyclerManager
    */
    private lateinit var adapter: ExerciseCategoriesAdapter


//------------------------------------------------------------------------
// LIFECYCLE FUNCTIONS
//------------------------------------------------------------------------

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        val view: View? = inflater.inflate(R.layout.fragment_exercise_categories, container, false)

        dbGym = GymDataBase.getInstance(activity?.applicationContext!!)!!

        Log.d(TAG, "onCreate() called")

        recyclerView = view?.findViewById(R.id.fragment_exercise_recycler_view) as RecyclerView
        linearLayoutManager = LinearLayoutManager(view.context, LinearLayout.VERTICAL,false)
        recyclerView.layoutManager = linearLayoutManager

        categories = ArrayList<ExerciseCategoryEntity>()

        adapter = ExerciseCategoriesAdapter(categories, activity as Activity )
        recyclerView.adapter = adapter

        defineQuery()
        setUpCategories()

        return view
    }

    override fun onResume() {
        super.onResume()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    override fun onPause() {
        super.onPause()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
    }

//------------------------------------------------------------------------
// SETUP FUNCTIONS
//------------------------------------------------------------------------

    /**
     *
     */
    private fun defineQuery( ){
        query = db.orderBy(CATEGORIES_NAME, Query.Direction.ASCENDING)
    }

    /**
     *
     */
    private fun setUpCategories(){
        if(ConectivityHelper.checkOverallConectivity(activity!!.applicationContext)){
            fetchCategories()
        }
        else{
            getStoredCategories()
        }

    }

//------------------------------------------------------------------------
// FIREBASE RELATED FUNCTIONS
//------------------------------------------------------------------------

    private fun fetchCategories(){
        query.get().addOnCompleteListener { task ->
            if(task.isSuccessful){
                task.result?.forEach { document ->
                    Log.d(TAG, "Categoria: ${document.get(CATEGORIES_NAME)}")
                    var category: ExerciseCategoryEntity = document.toObject(ExerciseCategoryEntity::class.java)
                    category.id = document.id
                    Log.d(TAG, "Categoria: ${category.id}")
                    categories.add(category)

                    adapter.notifyDataSetChanged()
                }

                saveCategoriesInStorage(categories)
            }
            else{
                throw Exception(task.exception)
            }
        }
    }

//------------------------------------------------------------------------
// ROOM DB RELATED FUNCTIONS
//------------------------------------------------------------------------
    /**
     * Saves and Updates categories in local storage
     */
    private fun saveCategoriesInStorage(catList: ArrayList<ExerciseCategoryEntity>){

        doAsync {
            var db = GymDataBase.getInstance(this@ExercisesFragment.activity!!.applicationContext)!!
            db.exerciseCategoryDao().insertAndUpdateExerciseCategories(catList)
            Log.d(TAG, "Categories persisted")
        }
    }

    /**
     *
     */
    private fun getStoredCategories(){
        doAsync {
            var db = GymDataBase.getInstance(this@ExercisesFragment.activity!!.applicationContext)!!
            var catList: List<ExerciseCategoryEntity>? = db.exerciseCategoryDao().getAllExerciseCategories()

            uiThread {
                if(catList != null && catList.isNotEmpty()){
                    catList.forEach{cat ->
                        categories.add(cat)
                    }
                    adapter.notifyDataSetChanged()
                }
                else{
                    tellUserAboutStorageQueriedData(ConectivityHelper.CONECTIVITY_ISSUES," No Es posible mostrar categorias")
                }
            }
        }
    }

//------------------------------------------------------------------------
// HELPER FUNCTIONS
//------------------------------------------------------------------------

    /**
     * Messages the user to indicate conectivity issues
     */
    private fun tellUserAboutStorageQueriedData(status: String, detail: String){
        Toast.makeText(activity!!.applicationContext, status + " " + detail, Toast.LENGTH_LONG).show()
    }

}
