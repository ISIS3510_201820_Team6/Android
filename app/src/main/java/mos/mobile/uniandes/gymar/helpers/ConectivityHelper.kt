package mos.mobile.uniandes.gymar.helpers

import android.content.Context
import android.net.NetworkInfo
import android.content.Context.CONNECTIVITY_SERVICE
import android.support.v4.content.ContextCompat.getSystemService
import android.net.ConnectivityManager
import android.util.Log


class ConectivityHelper {


    companion object {

        private val TAG = ConectivityHelper::class.java.simpleName

        const val CONECTIVITY_ISSUES = "No hay Conexion a Internet!"

        const val CONECTIVITY_ISSUES_WIFI = "There's no Wifi Connection!"

        fun checkOverallConectivity(context: Context): Boolean{
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
            val isConnected: Boolean = (activeNetwork != null) && activeNetwork.isConnectedOrConnecting == true

            val isWiFi: Boolean = activeNetwork?.type == ConnectivityManager.TYPE_WIFI

            Log.d(TAG, "Conectivity Exercise status: $isConnected$isWiFi")

            return (isConnected || ( isConnected && isWiFi))
        }
    }
}