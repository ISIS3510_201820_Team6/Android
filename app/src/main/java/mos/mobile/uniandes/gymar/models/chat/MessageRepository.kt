package mos.mobile.uniandes.gymar.models.chat

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import com.google.firebase.firestore.EventListener
import mos.mobile.uniandes.gymar.helpers.FireBaseDBConstants
import mos.mobile.uniandes.gymar.pojos.TextMessageItem
import java.text.SimpleDateFormat
import java.util.*

class MessageRepository {

    private val chatDB: CollectionReference

    private lateinit var query: Query

    private var mAuth: FirebaseAuth

    init {
        chatDB = FirebaseFirestore.getInstance().collection("Messages")
        mAuth = FirebaseAuth.getInstance()
    }

//------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------

    private var messages: MutableLiveData<MutableList<TextMessageItem>> = MutableLiveData()


    private lateinit var listenerFirestore: ListenerRegistration

//------------------------------------------------------------------------
// PUBLIC FUNCITONS
//------------------------------------------------------------------------

    fun getMessages(conectivity: Boolean, chatId: String): LiveData<MutableList<TextMessageItem>> {
        query = chatDB.whereEqualTo("idChat", chatId).orderBy("fecha", Query.Direction.ASCENDING)
        if (messages.value == null){
            messages.value = mutableListOf<TextMessageItem>()
            Log.d("HAYINTERNET", conectivity.toString())
            if (conectivity){
                fetchChatMessages(chatId)
            }
        }
        return messages
    }

//------------------------------------------------------------------------
// PRIVATE FUNCITONS
//------------------------------------------------------------------------

    private fun fetchChatMessages( chatId: String){
        var mensajes2 = messages.value
        if(mensajes2 != null){

            query.addSnapshotListener(EventListener<QuerySnapshot>{ querySnapshot, exception ->
                if(exception != null){
                    return@EventListener
                }
                else{
                    querySnapshot?.documentChanges?.forEach{document ->
                        when(document.type){
                            DocumentChange.Type.ADDED ->{
                                var documento = document.document
                                var mensaje = documento.getString("mensaje")
                                var idChat = chatId
                                var idSender = documento.getString("idSender")
                                var fecha = documento.getDate("fecha")
                                var fechaBonita = SimpleDateFormat("FF/MM/yyyy hh:mm  aa")
                                var fechita = fechaBonita.format(fecha)
                                var message = TextMessageItem(mensaje!!, fechita.toString(), idSender!!)
                                mensajes2.add(message)
                                messages.value = mensajes2
                                messages.postValue(mensajes2)
                            }
                            DocumentChange.Type.MODIFIED ->{

                            }
                            DocumentChange.Type.REMOVED ->{
                            }
                        }
                    }
                }
            })


        }

    }
}