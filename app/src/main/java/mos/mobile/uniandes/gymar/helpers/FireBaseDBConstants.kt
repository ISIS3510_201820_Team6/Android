package mos.mobile.uniandes.gymar.helpers

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.QueryDocumentSnapshot
import mos.mobile.uniandes.gymar.entities.ExerciseEntity
import mos.mobile.uniandes.gymar.entities.PlanEntity
import mos.mobile.uniandes.gymar.entities.RoutineEntity
import mos.mobile.uniandes.gymar.pojos.TrainerUser

class FireBaseDBConstants {

    companion object {

        /**
         * User's Collection Info
         */
        const val USERS_TABLE: String = "usuarios"

        /**
         * Users Plan's Collection Info
         */
        const val PLANS_USERS_TABLE: String = "usuario_plan"
        const val PLANS_USERS_BEGIN: String = "inicio"
        const val PLANS_USERS_PLAN: String = "plan"
        const val PLANS_USERS_USER: String = "usuario"
        const val PLANS_USERS_EXPIRE: String = "vencimiento"


        /**
         *
         */
        const val TRAINER_USER_TABLE: String = "trainerUser"
        const val TRAINER_USER_CLIENT: String = "cliente"
        const val TRAINER_USER_EXERCISE: String = "ejercicio"
        const val TRAINER_USER_TRAINER: String = "tId"
        const val TRAINER_USER_CLIENT_ID: String = "IDCliente"
        const val TRAINER_USER_DATE: String = "fechaSolicitud"
        const val TRAINER_USER_REALIZED: String = "realizada"
        const val TRAINER_USER_IMAGE: String = "imagen"
        const val TRAINER_USER_CONDITIONS: String = "condiciones"

        /**
         * Plan's Collection Info
         */
        const val PLANS_TABLE: String = "Plan"
        const val PLANS_NIVEL: String = "nivel"
        const val PLANS_OBJETIVO: String = "objetivo"
        const val PLANS_SEMANAS: String = "semanas"
        const val PLANS_DIAS: String = "diasSemana"
        const val PLAN_ROUTINES: String = "rutinas"

        /**
         * Routines's Collection Info
         */
        const val ROUTINES_TABLE: String = "rutinas"
        const val ROUTINES_NOMBRE: String = "nombre"
        const val ROUTINES_TIPO: String = "tipo"
        const val ROUTINES_IMAGEN: String = "imagen"

        const val ROUTINES_EXERCISES = "ejercicios"
        const val ROUTINES_EXERCISE = "ejercicio"
        const val ROUTINES_REPETICIONES = "numeroRepeticionesPorSerie"
        const val ROUTINES_SERIES = "numeroSeries"


        /**
         * Exercises's Collection Info
         */
        const val EXERCISES_TABLE: String = "ejercicios"
        const val EXERCISES_NAME: String  = "nombre"
        const val EXERCISE_CATEGORY: String  = "categoria"
        const val EXERCISES_DESCRIPTION: String  ="descripcion"
        const val EXERCISES_ICON_ANDRIOD: String  ="iconoAndroid"
        const val EXERCISES_ICON_IOS: String  ="iconoIOS"
        const val EXERCISES_FOTO: String  ="foto"
        const val EXERCISES_VIDEO: String  ="video"
        const val EXERCISES_AR: String  ="ar"
        const val EXERCISES_ZONE: String  ="zona"


        /**
         * Category's Collection Info
         */
        const val CATEGORIES_TABLE: String = "categorias"
        const val CATEGORIES_NAME: String = "nombre"
        const val CATEGORIES_ICON_ANDROID: String = "iconoAndroid"
        const val CATEGORIES_ICON_IOS: String = "iconoIOS"

        /**
         * My Body Statistics Collection
         */
        const val BODY_STATISTICS_TABLE: String = "bodyStatistics"
        const val BODY_STATISTICS_BRAZO: String = "brazo"
        const val BODY_STATISTICS_ANTEBRAZO: String = "antebrazo"
        const val BODY_STATISTICS_MUSLO: String = "muslo"
        const val BODY_STATISTICS_PANTORRILLA: String = "pantorrilla"
        const val BODY_STATISTICS_TORAX: String = "torax"
        const val BODY_STATISTICS_USER: String = "user"

        const val STATISTICS_TABLE:String = "statistics"
        const val STATISTICS_DATE:String = "fecha"
        const val STATISTICS_FAT:String = "grasa"
        const val STATISTICS_PERIMETER:String = "perimetro"
        const val STATISTICS_WEIGHT:String = "peso"
        const val STATISTICS_USER:String = "userId"


        /**
         * Search By Commands
         */
        const val GET_BY_CATEGORY = "BY_CATEGORY"
        const val GET_BY_ROUTINE = "BY_ROUTINE"

        /**
         * Maximum documents that can be returned by any query
         */
        const val MAX_ITEMS_PER_QUERY: Long = 13


        /**
         * Genera una instancia de la clase ExerciseEntity
         */
        fun createExerciseEntity( queryD: QueryDocumentSnapshot): ExerciseEntity {
            var id = queryD.id
            var nombre = queryD.get(EXERCISES_NAME) as String
            var categoria = queryD.get(EXERCISE_CATEGORY) as DocumentReference //Documento de Referencia
            var descripcion = queryD.get(EXERCISES_DESCRIPTION) as String
            var iconoAndroid = queryD.get(EXERCISES_ICON_ANDRIOD) as String
            var iconoIOS = queryD.get(EXERCISES_ICON_IOS) as String
            var foto = queryD.get(EXERCISES_FOTO) as String
            var video = queryD.get(EXERCISES_VIDEO) as String
            var ar = queryD.get(EXERCISES_AR) as String
            var zona = queryD.get(EXERCISES_ZONE) as String

            return ExerciseEntity(id,queryD.id, nombre,categoria.id,descripcion,iconoAndroid,iconoIOS,foto,video,ar, zona)
        }

        /**
         * Genera una instancia de la clase ExerciseEntity
         */
        fun createExerciseEntity( queryD: DocumentSnapshot): ExerciseEntity {
            var id = queryD.id
            var nombre = queryD.get(EXERCISES_NAME) as String
            var categoria = queryD.get(EXERCISE_CATEGORY) as DocumentReference //Documento de Referencia
            var descripcion = queryD.get(EXERCISES_DESCRIPTION) as String
            var iconoAndroid = queryD.get(EXERCISES_ICON_ANDRIOD) as String
            var iconoIOS = queryD.get(EXERCISES_ICON_IOS) as String
            var foto = queryD.get(EXERCISES_FOTO) as String
            var video = queryD.get(EXERCISES_VIDEO) as String
            var ar = queryD.get(EXERCISES_AR) as String
            var zona = queryD.get(EXERCISES_ZONE) as String

            return ExerciseEntity(id,queryD.id,nombre,categoria.id,descripcion,iconoAndroid,iconoIOS,foto,video,ar, zona)
        }

        /**
         *
         */
        fun createPlanEntity(document: DocumentSnapshot): PlanEntity {
            var nivel = document.get(PLANS_NIVEL) as String
            var obj = document.get(PLANS_OBJETIVO) as String
            var sem = document.get(PLANS_SEMANAS) as Long
            var dias = document.get(PLANS_DIAS) as Long
            var id = document.id

            return PlanEntity(id,nivel,obj,sem,dias)
        }

        /**
         *
         */
        fun createRoutineEntity(document: DocumentSnapshot, planEntity: PlanEntity): RoutineEntity {
            var nombre: String = document.get(ROUTINES_NOMBRE) as String
            var tipo: String = document.get(ROUTINES_TIPO) as String
            var imagen: String = document.get(ROUTINES_IMAGEN) as String
            var id: String = document.id

            return RoutineEntity(id, nombre, tipo, imagen, planEntity.id)
        }

        fun createTrainerUser(queryD: QueryDocumentSnapshot): TrainerUser{
            var cliente = queryD.get(TRAINER_USER_CLIENT) as String
            var ejercicio = queryD.get(TRAINER_USER_EXERCISE) as String
            var fecha = queryD.getDate(TRAINER_USER_DATE)
            var realizada = queryD.getBoolean(TRAINER_USER_REALIZED)
            var tId = queryD.get(TRAINER_USER_TRAINER) as String
            var cId = queryD.get(TRAINER_USER_CLIENT_ID) as String
            var imagen = queryD.get(TRAINER_USER_IMAGE) as String
            var id = queryD.id
            var condiciones = queryD.get(TRAINER_USER_CONDITIONS) as String
            var nombreEntrenador = queryD.get("nombreEntrenador") as String

            return TrainerUser(cliente, imagen, ejercicio, fecha!!, realizada!!, tId, cId, id, condiciones, nombreEntrenador)
        }
    }
}