package mos.mobile.uniandes.gymar.daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import mos.mobile.uniandes.gymar.entities.RoutineEntity

@Dao
interface RoutineDao: CommonDao<RoutineEntity> {

    @Query(value = "SELECT * FROM rutinas")
    fun getAllRoutines(): LiveData<List<RoutineEntity>>

    @Query(value = "SELECT * FROM rutinas WHERE planId = :planId")
    fun getRoutinesByPlan(planId: String): LiveData<List<RoutineEntity>>

    @Query(value = "DELETE FROM rutinas")
    fun deleteAll()

    @Transaction
    fun insertRoutines(rList: ArrayList<RoutineEntity>){
        deleteAll()
        rList.forEach { r ->
            insert(r)
        }
    }
}