package mos.mobile.uniandes.gymar.activities.statistics

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import mos.mobile.uniandes.gymar.R
import mos.mobile.uniandes.gymar.activities.exercises.ExerciseDetailActivity2
import mos.mobile.uniandes.gymar.activities.exercises.ExercisesAdapter
import mos.mobile.uniandes.gymar.entities.ExerciseEntity
import mos.mobile.uniandes.gymar.pojos.RoutineExercise
import org.jetbrains.anko.startActivity

class ExercisesRecommendationAdapter(private val exercises: MutableList<ExerciseEntity>?, private val activity: Activity): RecyclerView.Adapter<ExercisesRecommendationAdapter.ViewHolder>(){

    private val TAG = ExercisesAdapter::class.java.simpleName

    fun setExercisesByCategory(exerciseList: MutableList<ExerciseEntity>) {
        exercises?.clear()
        exercises?.addAll(exerciseList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): ExercisesRecommendationAdapter.ViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.activity_exercises_recyclerview_item_row, viewGroup, false)
        return ViewHolder(view, activity)
    }

    override fun getItemCount(): Int {
        var s = 0
        if(exercises != null){
            s = exercises.size
        }
        return s
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        if(exercises != null){
            val exerciseEntity: ExerciseEntity = exercises[position]

            //data binding with ViewHolder
            viewHolder.bindExerciseLayoutData(exerciseEntity)

            //Manejo de Clicks para nueva Activity
            viewHolder.itemView.setOnClickListener{
                activity.startActivity<ExerciseDetailActivity2>("EXERCISE_DETAIL" to exerciseEntity)
                Log.d(TAG, "Element Clicked")
            }
        }
    }

    /**
     * ViewHolder utilizado en el Adaptador
     */
    class ViewHolder(itemView: View, activity: Activity): RecyclerView.ViewHolder(itemView){

        /**
         * TextView of the Row Item Layout that contains the name of the exercise
         */
        private val exerciseNameText: TextView = itemView.findViewById<TextView>(R.id.activity_exercises_recyclerview_nameText)

        /**
         * TextView of the Row Item Layout that contains the description of the exercise
         */
        private val exerciseDescriptionText: TextView = itemView.findViewById<TextView>(R.id.activity_exercises_recyclerview_descriptionText)

        /**
         *
         */
        private val exercisesIcon: ImageView = itemView.findViewById<ImageView>(R.id.activity_exercises_recyclerview_icon)

        /**
         *
         */
        val options = RequestOptions().error(R.drawable.network_error)

        /**
         *
         */
        val glideRequestManager: RequestManager = Glide.with(activity)

        /**
         * Funcion para hacer binding entre llos componentes del layout y el objeto dado por parametro
         */
        fun bindExerciseLayoutData(exerciseEntity: ExerciseEntity){
            exerciseNameText.text = exerciseEntity.nombre
            exerciseDescriptionText.text = exerciseEntity.descripcion

            glideRequestManager.load(exerciseEntity.iconoAndroid).thumbnail(glideRequestManager.load(R.drawable.loading_icon)).apply(options).into(exercisesIcon)
        }

    }
}