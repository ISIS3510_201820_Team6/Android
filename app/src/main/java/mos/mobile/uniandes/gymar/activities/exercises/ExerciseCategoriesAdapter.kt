package mos.mobile.uniandes.gymar.activities.exercises


import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import mos.mobile.uniandes.gymar.R
import mos.mobile.uniandes.gymar.entities.ExerciseCategoryEntity
import org.jetbrains.anko.startActivity

/**
 * Adapter utilizado para el manejo de la lista de categorias de los ejercicios
 */
class ExerciseCategoriesAdapter(val exerciseCategoryEntities: ArrayList<ExerciseCategoryEntity>, private val activity: Activity): RecyclerView.Adapter<ExerciseCategoriesAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parentContext: ViewGroup, position: Int): ViewHolder {
        val view: View = LayoutInflater.from(parentContext.context).inflate(R.layout.activity_exercises_recyclerview_item_row, parentContext, false)
        return ViewHolder(view, activity)
    }

    override fun getItemCount(): Int {
        return exerciseCategoryEntities.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val exerciseCategoryEntity: ExerciseCategoryEntity = exerciseCategoryEntities[position]

        //Data binding for the category text in list
        viewHolder.bindExerciseCategoryLayoutData(exerciseCategoryEntity)

        viewHolder.itemView.setOnClickListener{
            activity.startActivity<ExerciseListByCategoryActivity>("EXERCISE_CATEGORY" to exerciseCategoryEntity.id, "EXERCISE_NAME" to exerciseCategoryEntity.nombre)
        }
    }

    /**
     * ViewHolder utilizado en el Adaptador
     */
    class ViewHolder(itemView: View, activity: Activity): RecyclerView.ViewHolder(itemView){
        /**
         * TextView of the Row Item Layout that contains the name of the exercise
         */
        private val exerciseNameText: TextView = itemView.findViewById<TextView>(R.id.activity_exercises_recyclerview_nameText)

        /**
         *
         */
        private val exercisesIcon: ImageView = itemView.findViewById<ImageView>(R.id.activity_exercises_recyclerview_icon)

        /**
         *
         */
        val options = RequestOptions().error(R.drawable.network_error)

        /**
         *
         */
        val glideRequestManager: RequestManager = Glide.with(activity)

        fun bindExerciseCategoryLayoutData(exerciseEntity: ExerciseCategoryEntity){
            exerciseNameText.text = exerciseEntity.nombre
            glideRequestManager.load(exerciseEntity.iconoAndroid).thumbnail(glideRequestManager.load(R.drawable.loading_icon)).apply(options).into(exercisesIcon)
        }
    }

}