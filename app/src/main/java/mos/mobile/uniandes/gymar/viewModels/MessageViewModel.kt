package mos.mobile.uniandes.gymar.viewModels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.util.Log
import mos.mobile.uniandes.gymar.helpers.ConectivityHelper
import mos.mobile.uniandes.gymar.models.chat.MessageRepository
import mos.mobile.uniandes.gymar.pojos.TextMessageItem

class MessageViewModel: ViewModel() {

    //------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------
    private lateinit var messages: LiveData<MutableList<TextMessageItem>>

    private var messageRepository: MessageRepository = MessageRepository( )

//------------------------------------------------------------------------
// PUBLIC FUNCITONS
//------------------------------------------------------------------------

    /**
     * Returns a liveData list with the exercises according to the specified parameters
     */
    fun getMessages(context: Context, chatId: String): LiveData<MutableList<TextMessageItem>> {
        if (!::messages.isInitialized) {
            messages = getChatMessages(context, chatId)
        }
        return messages
    }



//------------------------------------------------------------------------
// PRIVATE FUNCITONS
//------------------------------------------------------------------------

    private fun getChatMessages(context: Context, chatId: String): LiveData<MutableList<TextMessageItem>> {
        return messageRepository.getMessages(ConectivityHelper.checkOverallConectivity(context), chatId)
    }
}