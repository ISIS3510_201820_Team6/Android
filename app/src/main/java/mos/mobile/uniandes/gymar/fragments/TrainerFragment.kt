package mos.mobile.uniandes.gymar.fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.google.firebase.firestore.FirebaseFirestore

import mos.mobile.uniandes.gymar.R
import mos.mobile.uniandes.gymar.activities.chat.ChatActivity
import mos.mobile.uniandes.gymar.helpers.ConectivityHelper
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [TrainerFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [TrainerFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class TrainerFragment : Fragment() {

    lateinit var nombreEntrenador: String
    lateinit var nombreCliente: String
    lateinit var idEntrenador: String
    lateinit var idCliente: String
    private val chatDB = FirebaseFirestore.getInstance().collection("chats")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_trainer, container, false)
        nombreEntrenador = this.arguments!!.getString("TRAINER_NAME")
        nombreCliente = this.arguments!!.getString("CLIENT_NAME")
        idEntrenador = this.arguments!!.getString("TRAINER_ID")
        idCliente = this.arguments!!.getString("CLIENT_ID")

        var nombreTrainerView = view.findViewById<TextView>(R.id.trainer_fragment_nombre)
        nombreTrainerView.text = nombreEntrenador
        // Inflate the layout for this fragment

        var chatButton = view.findViewById<Button>(R.id.enter_chat_button)
        chatButton.setOnClickListener { onButtonClick() }
        return view
    }

    fun onButtonClick(){
        if(ConectivityHelper.checkOverallConectivity(context!!)){
            val nameEntrenador = nombreEntrenador
            val nameClient = nombreCliente
            var query = chatDB.whereEqualTo("idEntrenador", idEntrenador).whereEqualTo("idCliente", idCliente)
            query.get().addOnCompleteListener { task ->
                if(task.isSuccessful){
                    var result = task.result
                    if(result != null){
                        if(result.isEmpty){
                            //Crear Documento
                            var solicitud = HashMap<String, Any?>()
                            solicitud.put("idEntrenador", idEntrenador)
                            solicitud.put("idCliente", idCliente)
                            solicitud.put("nombreCliente", nameClient)
                            solicitud.put("nombreEntrenador", nameEntrenador)
                            chatDB.add(solicitud).addOnCompleteListener { task ->
                                var resultado = task.result
                                var id = resultado?.id
                                val intent = Intent(context, ChatActivity::class.java)
                                intent.putExtra("CHAT_ID", id)
                                intent.putExtra("TRAINER_NAME", nameEntrenador)
                                intent.putExtra("CLIENT_NAME", nameClient)
                                intent.putExtra("TRAINER_ID", idEntrenador)
                                intent.putExtra("CLIENT_ID", idCliente)
                                startActivity(intent)
                            }
                        }
                        else{
                            var list = result.documents
                            var chat = list[0]
                            var chatId = chat.id
                            var idEntrenador = idEntrenador
                            var idCliente = idCliente
                            var nameClient = chat.getString("nombreCliente")
                            var nameEntrenador = chat.getString("nombreEntrenador")
                            val intent = Intent(context, ChatActivity::class.java)
                            intent.putExtra("CHAT_ID", chatId)
                            intent.putExtra("TRAINER_NAME", nameEntrenador)
                            intent.putExtra("CLIENT_NAME", nameClient)
                            intent.putExtra("TRAINER_ID", idEntrenador)
                            intent.putExtra("CLIENT_ID", idCliente)
                            startActivity(intent)
                        }
                    }
                    else{
                        Log.d("QUERY", result.toString())
                    }
                }
            }
        }
        else{
            Snackbar.make(view!!.findViewById<LinearLayout>(R.id.button_complete), "No puedes acceder al chat sin internet", Snackbar.LENGTH_LONG).show()
        }
    }
}
