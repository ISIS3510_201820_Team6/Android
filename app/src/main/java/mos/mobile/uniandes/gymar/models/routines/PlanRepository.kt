package mos.mobile.uniandes.gymar.models.routines

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.util.Log
import com.google.firebase.firestore.ListenerRegistration
import mos.mobile.uniandes.gymar.entities.PlanEntity
import mos.mobile.uniandes.gymar.entities.RoutineEntity
import mos.mobile.uniandes.gymar.models.GymDataBase

class PlanRepository(context: Context) {

//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------

    private val TAG: String = PlanRepository::class.java.simpleName

    private val gymDB: GymDataBase

    init {
        gymDB = GymDataBase.getInstance(context)!!
    }

//------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------

    private var routines: LiveData<List<RoutineEntity>> = MutableLiveData()

    private lateinit var plans: LiveData<List<PlanEntity?>>

    private lateinit var listenerFirestore: ListenerRegistration

//------------------------------------------------------------------------
// PUBLIC FUNCTIONS
//------------------------------------------------------------------------

    /**
     * Gets the exercises according to a specific category
     */
    fun getPlan( ): LiveData<List<PlanEntity?>> {
        if(!::plans.isInitialized) {
            Log.d(TAG, "Fetching Locally")
            loadPlanFromStorage( )
        }
        return plans
    }

    fun getRoutines( ): LiveData<List<RoutineEntity>> {
        if(routines.value == null){
            Log.d(TAG, "Fetching Remotely")
            fetchRoutinesByCategory(  )
        }
        return routines
    }

    /**
     * Removes the listener when it is no longer needed
     */
    fun detachPosibleListeners(){
        if(::listenerFirestore.isInitialized){
            listenerFirestore.remove()
        }
    }

//------------------------------------------------------------------------
// PRIVATE FUNCITONS
//------------------------------------------------------------------------

    fun loadPlanFromStorage( ){
        plans = gymDB.planDao().getAllPlanEntities( )
    }

    fun fetchRoutinesByCategory( ){
        routines = gymDB.routineDao().getAllRoutines( )
    }
}
