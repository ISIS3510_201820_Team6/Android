package mos.mobile.uniandes.gymar


import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.google.firebase.auth.FirebaseAuth
import com.newrelic.agent.android.NewRelic;
import mos.mobile.uniandes.gymar.fragments.ExercisesFragment
import mos.mobile.uniandes.gymar.fragments.ProfileFragment
import mos.mobile.uniandes.gymar.fragments.ProgramFragment
import org.jetbrains.anko.alert


class MainActivity : AppCompatActivity() {

//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------

    /**
     * Llaves para el manejo de estados
     */
    companion object {
        private val ACTIVE_NAVEGATION_ITEM_ID = "ACTIVE_NAVEGATION_ITEM_ID"
    }

    /**
     * TAG para el uso de Logs en la aplicacion
     */
    private val TAG = MainActivity::class.java.simpleName

    /**
     *
     */
    internal val manager = supportFragmentManager

    /**
     * Firebase Auth
     */
    private lateinit var mAuth: FirebaseAuth

//------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------

    /**
     * Variable para manejar el identificador el menuItem seleccionado
     */
    private var activeNavegationItem: Int = 0

    /**
     * Navegation Bottom
     */
    internal lateinit var bottomNavigation: BottomNavigationView

//------------------------------------------------------------------------
// LIFECYCLE FUNCTIONS
//------------------------------------------------------------------------

    /**
     * Metodo que maneja el estado de creacion de la aplicacion
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mAuth = FirebaseAuth.getInstance()

        NewRelic.withApplicationToken(
                "AA2139bcdc6bfeafbd2bd044de666fb41eef45f1df"
        ).start(this.getApplication())

        //Log de verificacion en logcat
        Log.d(TAG, "OnCreate() method called." )

        //Se obtiene la barra de navegacion
        bottomNavigation = findViewById<BottomNavigationView>(R.id.bottom_navigation)

        //Listener para manejar eventos de navegacion
        bottomNavigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

        var toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        initialFragment(savedInstanceState)
    }

    /**
     * Metodo para guardar el estado del activity
     */
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        //Log de verificacion en Logcat
        Log.d(TAG, "[onSaveInstanceState] Active Navigation Item: $activeNavegationItem")

        outState?.putInt(ACTIVE_NAVEGATION_ITEM_ID, activeNavegationItem)
    }

//------------------------------------------------------------------------
// FRAGMENT MANAGEMENT FUNCTIONS
//------------------------------------------------------------------------

    /**
     * Listener del Bottom Navigation
     */
    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        Log.d(TAG, "Navegation Item Selected: ${item.itemId}")

        when(item.itemId) {
            R.id.access_exercises -> {
                activeNavegationItem = item.itemId
                openFragment(ExercisesFragment())
                true
            }
            R.id.access_profile -> {
                activeNavegationItem = item.itemId
                openFragment(ProfileFragment())
                true
            }
            R.id.access_program -> {
                activeNavegationItem = item.itemId
                openFragment(ProgramFragment())
                true
            }
            else -> false //TODO: verificar que sea false
        }
    }

    /**
     * Helper
     */
    private fun openFragment(fragment: Fragment) {
        val transaction = manager.beginTransaction()
        transaction.replace(R.id.fragment_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    /**
     * Asignacion del fragmento inicial en Oncreate
     */
    private fun initialFragment(savedInstanceState: Bundle?){
        if (savedInstanceState != null){
            with(savedInstanceState){
                activeNavegationItem = getInt(ACTIVE_NAVEGATION_ITEM_ID)
            }
        }
        else{
            activeNavegationItem = R.id.access_profile
        }

        //Asignacion del elemento seleccionado en el menu
        bottomNavigation.selectedItemId = activeNavegationItem
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.logout_option -> {
            // User chose the "Settings" item, show the app settings UI...
            alert("¿Está seguro de que desea cerrar sesión?") {
                title = "Cerrar Sesión"
                positiveButton("Sí") { logout() }
                negativeButton("Cancelar") { }
            }.show().apply {
                getButton(AlertDialog.BUTTON_POSITIVE)?.let { it.setTextColor(Color.BLACK) }
                getButton(AlertDialog.BUTTON_NEGATIVE)?.let { it.setTextColor(Color.BLACK) }
            }

            true
        }

        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

    fun logout(){
        mAuth.signOut()
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

}
