package mos.mobile.uniandes.gymar.viewModels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.util.Log
import mos.mobile.uniandes.gymar.entities.UserEntity
import mos.mobile.uniandes.gymar.helpers.ConectivityHelper
import mos.mobile.uniandes.gymar.models.exercises.ExercisesByCategoryRepository
import mos.mobile.uniandes.gymar.models.trainer.TrainerRepository
import mos.mobile.uniandes.gymar.pojos.TrainerUser

class TrainerViewModel: ViewModel() {


//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------
    private val TAG: String = ExercisesByCategoryViewModel::class.java.simpleName

//------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------
    private lateinit var clients: LiveData<MutableList<TrainerUser>>

    private var trainerRepository: TrainerRepository = TrainerRepository( )

//------------------------------------------------------------------------
// PUBLIC FUNCITONS
//------------------------------------------------------------------------

    /**
     * Returns a liveData list with the exercises according to the specified parameters
     */
    fun getClients(context: Context): LiveData<MutableList<TrainerUser>> {
        Log.d(TAG, "Getting Exercises")
        if (!::clients.isInitialized) {
            Log.d(TAG, "Fetching exercises")
            clients = getClientsSeekingHelp(context)
        }
        return clients
    }

    fun updateClientStatus(context: Context, trainerUser: TrainerUser){
        trainerRepository.updateClient(ConectivityHelper.checkOverallConectivity(context), trainerUser)
    }

    /**
     * Detaches the listeners that might have been initialized
     */
    fun detachListeners(){
        trainerRepository.detachPosibleListeners()
    }

//------------------------------------------------------------------------
// PRIVATE FUNCITONS
//------------------------------------------------------------------------

    private fun getClientsSeekingHelp(context: Context): LiveData<MutableList<TrainerUser>> {
        return trainerRepository.getClientsLookingForHelp(ConectivityHelper.checkOverallConectivity(context))
    }

}