package mos.mobile.uniandes.gymar.activities.exercises

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import mos.mobile.uniandes.gymar.R
import mos.mobile.uniandes.gymar.pojos.RoutineExercise
import mos.mobile.uniandes.gymar.viewModels.ExercisesByRoutineViewModel

class ExerciseListByRoutineActivity: AppCompatActivity() {

//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------
    /**
     * TAG para el uso de Logs en la aplicacion
     */
    private val TAG = ExerciseListByRoutineActivity::class.java.simpleName

//------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------

    /**
     *  Linear layout manager
     */
    private lateinit var linearLayoutManager: LinearLayoutManager

    /**
     * recyclerView
     */
    private lateinit var recyclerView: RecyclerView

    /**
     * Adapter for the recyclerManager
     */
    private lateinit var adapter: ExercisesAdapter

    /**
     *
     */
    private lateinit var viewModel: ExercisesByRoutineViewModel
//------------------------------------------------------------------------
// LIFECYCLE FUNCTIONS
//------------------------------------------------------------------------

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exercises)
        var title = intent.getStringExtra("ROUTINE_NAME")
        var toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.setTitle(title)
        setSupportActionBar(toolbar)
        //Enable back button on action bar.
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        //Defines Elements of the Activity/View
        recyclerView = findViewById(R.id.activity_exercises_recyclerview)
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        adapter = ExercisesAdapter(null, ArrayList<RoutineExercise>(), this)
        recyclerView.adapter = adapter

        //Defines the corresponding View Mod
        viewModel = ViewModelProviders.of(this).get(ExercisesByRoutineViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        viewModel.getExercises(this.applicationContext, intent.getStringExtra("EXERCISE_ROUTINE"))
                .observe(this, Observer { exercises ->
                    Log.d(TAG, "Exercises: $exercises")
                    if(exercises != null && exercises.isNotEmpty()){
                        adapter.setExercisesByRoutine(exercises)
                    }
                    else{
                        Log.e(TAG, "Error while updating Routine Exercises LiveData List")
                    }
                })
    }

    override fun onStop() {
        super.onStop()
        viewModel.detachListeners()
    }

//------------------------------------------------------------------------
// SETUP FUNCTIONS
//------------------------------------------------------------------------

    /**
     * Helper function for back button in action bar
     */
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id: Int? = item?.itemId
        if(id == android.R.id.home){
            this.finish()
        }
        return super.onOptionsItemSelected(item)
    }

}

