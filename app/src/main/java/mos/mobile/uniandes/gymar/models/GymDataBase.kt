package mos.mobile.uniandes.gymar.models

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import mos.mobile.uniandes.gymar.daos.*
import mos.mobile.uniandes.gymar.entities.*

@Database(entities = [(PlanEntity::class),(RoutineEntity::class), (ExerciseCategoryEntity::class), (RoutineExerciseEntity::class), (ExerciseEntity::class)], version = 2, exportSchema = false)
abstract  class GymDataBase: RoomDatabase() {
    abstract fun planDao(): PlanDao
    abstract fun routineDao(): RoutineDao
    abstract fun exerciseDao(): ExerciseDao
    abstract fun routineExerciseDao(): RoutineExerciseDao
    abstract fun exerciseCategoryDao(): ExerciseCategoryDao

    companion object {
        private var INSTANCE: GymDataBase? = null

        fun getInstance(context: Context): GymDataBase? {
            if (INSTANCE == null) {
                synchronized(GymDataBase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            GymDataBase::class.java, "weather.db")
                            .fallbackToDestructiveMigration()
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

}