package mos.mobile.uniandes.gymar.viewModels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.util.Log
import mos.mobile.uniandes.gymar.helpers.ConectivityHelper
import mos.mobile.uniandes.gymar.models.exercises.ExerciseRepository
import mos.mobile.uniandes.gymar.models.exercises.ExercisesByRoutineRepository
import mos.mobile.uniandes.gymar.pojos.RoutineExercise

class ExercisesByRoutineViewModel: ViewModel()  {

//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------
    private val TAG: String = ExercisesByRoutineViewModel::class.java.simpleName

    //------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------
    private lateinit var exercises: LiveData<MutableList<RoutineExercise>>

    private lateinit var exerciseRepository: ExerciseRepository<RoutineExercise>
//------------------------------------------------------------------------
// PUBLIC FUNCITONS
//------------------------------------------------------------------------

    /**
     * Returns a liveData list with the exercises according to the specified parameters
     */
    fun getExercises(context: Context, searchParam: String): LiveData<MutableList<RoutineExercise>> {
        Log.d(TAG, "Starting Routine ViewModel")
        if(!::exercises.isInitialized){
            Log.d(TAG, "Getting Exercises for ViewModel")
            exercises = loadExercises(context, searchParam)
        }
        return exercises
    }

    /**
     * Detaches the listeners that might have been initialized
     */
    fun detachListeners(){
        exerciseRepository.detachPosibleListeners()
    }

    /**
     * Returns a LiveData list with the exercises depending on the connectivity of the device
     */
    private fun loadExercises(context: Context, searchParam: String): LiveData<MutableList<RoutineExercise>> {
        exerciseRepository = ExercisesByRoutineRepository(context)
        return exerciseRepository.getExercises(ConectivityHelper.checkOverallConectivity(context), searchParam)
    }

}