package mos.mobile.uniandes.gymar.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

//Entity that describes the Routine DB scheme
@Parcelize
@Entity(tableName = "planes")
data class PlanEntity(
        @PrimaryKey @ColumnInfo(name = "id") var id: String,
        @ColumnInfo(name = "nivel") var nivel: String,
        @ColumnInfo(name = "objetivo") var objetivo: String,
        @ColumnInfo(name = "semanas") var semanas: Long,
        @ColumnInfo(name = "dias") var dias: Long): Parcelable{

    @Ignore
    constructor():this("", "","",0,0)
}