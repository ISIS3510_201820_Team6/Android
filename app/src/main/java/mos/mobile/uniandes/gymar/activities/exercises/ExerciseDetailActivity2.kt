package mos.mobile.uniandes.gymar.activities.exercises


import android.content.Intent
import android.content.pm.ActivityInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView
import mos.mobile.uniandes.gymar.R
import mos.mobile.uniandes.gymar.entities.ExerciseEntity
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AlertDialog
import android.support.v7.widget.Toolbar
import android.util.Log
import android.widget.Button
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import com.google.firebase.firestore.EventListener
import mos.mobile.uniandes.gymar.activities.IntermediateActivity
import mos.mobile.uniandes.gymar.fragments.TrainerFragment
import mos.mobile.uniandes.gymar.helpers.ConectivityHelper
import mos.mobile.uniandes.gymar.pojos.TrainerUser
import java.util.*


class ExerciseDetailActivity2 : AppCompatActivity() {

    lateinit var exercise: ExerciseEntity
    private lateinit var mAuth: FirebaseAuth
    lateinit var fragmentManager : FragmentManager
    private var activeTrainer : Boolean = false
    private var trainerUserDb: CollectionReference = FirebaseFirestore.getInstance().collection("trainerUser")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exercise_detail2)
        mAuth = FirebaseAuth.getInstance()
        //Get exercise from intent
        exercise = intent.getParcelableExtra("EXERCISE_DETAIL")

        var toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.setTitle(exercise.nombre)
        setSupportActionBar(toolbar)

        //Enable back button on action bar.
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        fragmentManager = supportFragmentManager
        // Set the text values
        var name = exercise?.nombre
        var description = exercise?.descripcion
        var link = exercise?.foto
        var steps = description?.split("@")
        var textView = findViewById<TextView>(R.id.exercise_description)
        findViewById<TextView>(R.id.exercise_title).text = name
        textView.text = ""

        for(step: String? in steps.orEmpty()){
            textView.append(step)
            textView.append(System.getProperty("line.separator"))
        }

        var video = exercise.video

        var player = findViewById<YouTubePlayerView>(R.id.youtube_player)
        lifecycle.addObserver(player)

        player.initialize({ initializedYouTubePlayer ->
            initializedYouTubePlayer.addListener(object : AbstractYouTubePlayerListener() {
                override fun onReady() {
                    val videoId = video
                    initializedYouTubePlayer.loadVideo(videoId, 0f)
                    initializedYouTubePlayer.pause()

                }
            })
        }, true)


        //Set button listeners
        //Call trainer button
        val callButton = findViewById<FloatingActionButton>(R.id.call_trainer_button)
        callButton.setOnClickListener { view ->
            callTrainer()
        }

        //Enter AR Button
        val arButton = findViewById<FloatingActionButton>(R.id.ar_button)
        arButton.setOnClickListener { view ->
            enterAR()
        }

        //Load the image
        val imageView = findViewById<ImageView>(R.id.exercise_image)
        val options = RequestOptions().error(R.drawable.network_error).fallback(R.drawable.network_error)
       Glide.with(this).load(link).thumbnail(Glide.with(this).load(R.drawable.loading_icon)).apply(options).into(imageView)

    }

    override fun onResume() {
        super.onResume()
        requestedOrientation =  ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        checkActiveTrainer()
    }

    override fun onPause() {
        super.onPause()
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
    }



    //Helper function for back button in action bar
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id: Int? = item?.itemId
        if(id == android.R.id.home){
            this.finish()
        }
        return super.onOptionsItemSelected(item)
    }

    fun callTrainer(){
        if(activeTrainer == false) {
            if(ConectivityHelper.checkOverallConectivity(this)){
                val usuario = mAuth.currentUser
                val db = FirebaseFirestore.getInstance().collection("solicitudesEntrenadores")
                var solicitud = HashMap<String, Any?>()
                FirebaseFirestore.getInstance().collection("usuarios").document(usuario!!.uid).get().addOnCompleteListener { task ->
                    if(task.isSuccessful){
                        val user = task.result
                        solicitud.put("cliente", user?.getString("nombre"))
                        solicitud.put("imagen", user?.getString("imagen"))
                        solicitud.put("ejercicio", exercise.nombre)
                        solicitud.put("realizada", false)
                        solicitud.put("zona", exercise.zona)
                        solicitud.put("fechaSolicitud", java.util.Date())
                        solicitud.put("IDCliente",usuario?.uid)

                        db.add(solicitud)
                                .addOnSuccessListener {
                                    val builder = AlertDialog.Builder(this)
                                    builder.setMessage("Un entrenador está corriendo hacia donde tu estás")
                                    builder.setTitle("En Camino")

                                    val dialog = builder.create()
                                    dialog.show()
                                }
                                .addOnFailureListener {
                                    val builder = AlertDialog.Builder(this)
                                    builder.setMessage("No pudimos encontrar un entrenador, intenta mas tarde")
                                    builder.setTitle("Error")

                                    val dialog = builder.create()
                                    dialog.show()
                                }

                    }
                }
            }
            else{
                val builder = AlertDialog.Builder(this)
                builder.setMessage("Necesitas conexión a internet para llamar a un entrenador")
                builder.setTitle("Sin internet")

                val dialog = builder.create()
                dialog.show()
            }
        }
        else{
            Snackbar.make(findViewById<Button>(R.id.call_trainer_button), "Ya tienes una solicitud de ayuda asignada", Snackbar.LENGTH_LONG)
        }



    }

    fun enterAR(){
        val intent = packageManager.getLaunchIntentForPackage("com.gymar.vuf")
        if(intent != null){
            startActivity(intent)
        }
        else{
            val builder = AlertDialog.Builder(this)
            builder.setMessage("Para poder acceder a esta funcionalidad debes descargar nuestra aplicación de realidad aumentada.")
            builder.setTitle("Instala la app de realidad aumentada")

            val dialog = builder.create()
            dialog.show()
        }

    }

    fun checkActiveTrainer(){
        var usuario = mAuth.currentUser
        if(usuario != null){
            var query = trainerUserDb.whereEqualTo("IDCliente",usuario.uid).whereEqualTo("realizada", false)
            query.addSnapshotListener(EventListener<QuerySnapshot>{ snapshots, e ->
                if(e != null){
                    return@EventListener
                }
                else{
                    snapshots?.documentChanges?.forEach {document ->
                        when(document.type){
                            DocumentChange.Type.ADDED -> {
                                val snapshot = document.document
                                var idEntrenador = snapshot.getString("tId")
                                var nombreEntrenador = snapshot.getString("nombreEntrenador")
                                var nombreCliente = snapshot.getString("cliente")
                                var trainerUserId = snapshot.id
                                var idCliente = snapshot.getString("IDCliente")
                                var transaction = fragmentManager.beginTransaction()
                                var fragmento = TrainerFragment()
                                var bundle = Bundle()
                                bundle.putString("TRAINER_NAME", nombreEntrenador)
                                bundle.putString("CLIENT_NAME", nombreCliente)
                                bundle.putString("TRAINER_ID", idEntrenador)
                                bundle.putString("CLIENT_ID", idCliente)
                                fragmento.arguments = bundle
                                transaction.add(R.id.floating_layout2, fragmento, "ENTRENADOR")
                                transaction.commitAllowingStateLoss()
                                activeTrainer = true
                            }
                            DocumentChange.Type.MODIFIED ->{
                                val snapshot = document.document
                                val condicion = snapshot.getBoolean("realizada") == true
                                if (condicion){
                                    var transaction = fragmentManager.beginTransaction()
                                    var f = fragmentManager.findFragmentByTag("ENTRENADOR")
                                    if(f != null){
                                        transaction.remove(f)
                                        transaction.commitAllowingStateLoss()
                                    }
                                }
                                activeTrainer = false
                            }
                            DocumentChange.Type.REMOVED ->{
                                Log.d("CHAO", "Removed!")
                            }
                        }

                    }
                }
            })
        }

    }




}


