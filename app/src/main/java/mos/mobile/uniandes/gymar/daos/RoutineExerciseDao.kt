package mos.mobile.uniandes.gymar.daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import mos.mobile.uniandes.gymar.entities.RoutineExerciseEntity

@Dao
interface RoutineExerciseDao: CommonDao<RoutineExerciseEntity> {

    @Query(value = "SELECT * FROM rutina_ejercicio")
    fun getAllRoutineExercises( ): List<RoutineExerciseEntity>

    @Query(value = "SELECT * FROM rutina_ejercicio WHERE routineId = :rutinaId")
    fun getAllRoutineExerciseEnttiesByRoutineId(rutinaId: String): LiveData<List<RoutineExerciseEntity>>

    @Query(value = "SELECT * FROM rutina_ejercicio WHERE routineId = :rutinaId AND exerciseId = :ejercicio")
    fun getAllRoutineExerciseEntitiesByRoutineIdAndExerciseId(rutinaId: String, ejercicio: String): List<RoutineExerciseEntity>

    @Query(value = "DELETE FROM rutina_ejercicio")
    fun deleteAll()
}