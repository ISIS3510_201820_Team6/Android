package mos.mobile.uniandes.gymar.activities.trainers

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import mos.mobile.uniandes.gymar.R
import mos.mobile.uniandes.gymar.activities.chat.ChatActivity
import mos.mobile.uniandes.gymar.helpers.ConectivityHelper
import java.util.HashMap

class UserDetailActivity : AppCompatActivity() {

//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------

    /**
     * TAG para el uso de Logs en la aplicacion
     */
    private val TAG = UserDetailActivity::class.java.simpleName

//------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------

    private lateinit var imagen: ImageView

    private lateinit var nombreEjercicio: TextView

    private lateinit var condiciones: TextView

    private lateinit var terminarBtn: Button

    private lateinit var chatBtn: Button

    private val chatDB = FirebaseFirestore.getInstance().collection("chats")

    private lateinit var mAuth: FirebaseAuth

//------------------------------------------------------------------------
// LIFECYCLE FUNTIONS
//------------------------------------------------------------------------

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_detail)

        mAuth = FirebaseAuth.getInstance()

        var title = intent.getStringExtra("USER_NAME")
        var toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.setTitle(title)
        setSupportActionBar(toolbar)

        //Enable back button on action bar.
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        //Set button listeners
        val chatButton = findViewById<Button>(R.id.button_chat)
        chatButton.setOnClickListener { onChatClick() }

        imagen = findViewById(R.id.user_profile_image)
        nombreEjercicio = findViewById(R.id.user_profile_exercise)
        condiciones = findViewById(R.id.user_profile_conditions)

        terminarBtn = findViewById(R.id.button_complete)
        chatBtn = findViewById(R.id.button_chat)

        var glideOptions = RequestOptions().placeholder(R.drawable.person_orange_24dp).error(R.drawable.person_orange_24dp)
        Glide.with(this).setDefaultRequestOptions(glideOptions).load(intent.getStringExtra("USER_IMAGE")).into(imagen)

        nombreEjercicio.text = intent.getStringExtra("USER_EXERCISE")
        intent.getStringExtra("USER_CONDITIONS").split("\n").forEach { word ->
            condiciones.append(word)
        }
    }

//------------------------------------------------------------------------
// SETUP FUNCTIONS
//------------------------------------------------------------------------

    //Helper function for back button in action bar
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id: Int? = item?.itemId
        if(id == android.R.id.home){
            this.finish()
        }
        return super.onOptionsItemSelected(item)
    }

    fun onChatClick(){
        if(ConectivityHelper.checkOverallConectivity(this)){
            val nameEntrenador = intent.getStringExtra("TRAINER_NAME")
            val nameClient = intent.getStringExtra("USER_NAME")
            val idCliente = intent.getStringExtra("USER_ID")
            val idEntrenador = mAuth.currentUser?.uid
            var query = chatDB.whereEqualTo("idEntrenador", idEntrenador).whereEqualTo("idCliente", idCliente)
            query.get().addOnCompleteListener { task ->
                if(task.isSuccessful){
                    var result = task.result
                    if(result != null){
                        if(result.isEmpty){
                            //Crear Documento
                            var solicitud = HashMap<String, Any?>()
                            solicitud.put("idEntrenador", idEntrenador)
                            solicitud.put("idCliente", idCliente)
                            solicitud.put("nombreCliente", nameClient)
                            solicitud.put("nombreEntrenador", nameEntrenador)
                            chatDB.add(solicitud).addOnCompleteListener { task ->
                                var resultado = task.result
                                var id = resultado?.id
                                val intent = Intent(this, ChatActivity::class.java)
                                intent.putExtra("CHAT_ID", id)
                                intent.putExtra("TRAINER_NAME", nameEntrenador)
                                intent.putExtra("CLIENT_NAME", nameClient)
                                intent.putExtra("TRAINER_ID", idEntrenador)
                                intent.putExtra("CLIENT_ID", idCliente)
                                startActivity(intent)
                            }
                        }
                        else{
                            var list = result.documents
                            var chat = list[0]
                            var chatId = chat.id
                            var idEntrenador = chat.getString("idEntrenador")
                            var idCliente = chat.getString("idCliente")
                            var nameClient = chat.getString("nombreCliente")
                            var nameEntrenador = chat.getString("nombreEntrenador")
                            val intent = Intent(this, ChatActivity::class.java)
                            intent.putExtra("CHAT_ID", chatId)
                            intent.putExtra("TRAINER_NAME", nameEntrenador)
                            intent.putExtra("CLIENT_NAME", nameClient)
                            intent.putExtra("TRAINER_ID", idEntrenador)
                            intent.putExtra("CLIENT_ID", idCliente)
                            startActivity(intent)
                        }
                    }
                    else{
                        Log.d("QUERY", result.toString())
                    }
                }
            }
        }
        else{
            Snackbar.make(findViewById<LinearLayout>(R.id.button_complete), "No puedes acceder al chat sin internet", Snackbar.LENGTH_LONG).show()
        }

    }
}
