package mos.mobile.uniandes.gymar.activities.statistics

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.ActivityInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import com.github.mikephil.charting.charts.HorizontalBarChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import mos.mobile.uniandes.gymar.R
import mos.mobile.uniandes.gymar.entities.ExerciseEntity
import mos.mobile.uniandes.gymar.helpers.FireBaseDBConstants
import mos.mobile.uniandes.gymar.helpers.chartFormatters.MyBodyAxisValueFormatter
import mos.mobile.uniandes.gymar.viewModels.ExercisesRecommendationViewModel
import mos.mobile.uniandes.gymar.viewModels.MyBodyViewModel

class MyBodyActivity : AppCompatActivity() {

//------------------------------------------------------------------------
// VALUES
//------------------------------------------------------------------------
    /**
     * TAG para el uso de Logs en la aplicacion
     */
    private val TAG = MyBodyActivity::class.java.simpleName

    private val xLabels: Array<String> = arrayOf(FireBaseDBConstants.BODY_STATISTICS_BRAZO,
            FireBaseDBConstants.BODY_STATISTICS_ANTEBRAZO, FireBaseDBConstants.BODY_STATISTICS_MUSLO,
            FireBaseDBConstants.BODY_STATISTICS_PANTORRILLA, FireBaseDBConstants.BODY_STATISTICS_TORAX)

    private val colors: IntArray = arrayOf(R.color.body_initial, R.color.body_actual, R.color.body_objective).toIntArray()

//------------------------------------------------------------------------
// VARIABLES
//------------------------------------------------------------------------

    /**
    *  Linear layout manager
    */
    private lateinit var linearLayoutManager: LinearLayoutManager

    /**
     * recyclerView
     */
    private lateinit var recyclerView: RecyclerView

    /**
     * Adapter for the recyclerManager
     */
    private lateinit var adapter: ExercisesRecommendationAdapter

    /**
     * Graph
     */
    private lateinit var barChart: HorizontalBarChart

    /**
     * ViewModel related to graphing data in the activity
     */
    private lateinit var myBodyViewModel: MyBodyViewModel

    /**
     * ViewModel related to recommendations in the activity
     */
    private lateinit var exercisesRecommendationViewModel: ExercisesRecommendationViewModel

//------------------------------------------------------------------------
// LIFECYCLE FUNCTIONS
//------------------------------------------------------------------------

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_body)
        var toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.setTitle("Condición Física")
        setSupportActionBar(toolbar)
        //Enable back button on action bar.
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        recyclerView = findViewById(R.id.activity_mybody_recyclerView)
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        adapter = ExercisesRecommendationAdapter(mutableListOf<ExerciseEntity>(), this)
        recyclerView.adapter = adapter

        myBodyViewModel = ViewModelProviders.of(this).get(MyBodyViewModel::class.java)
        exercisesRecommendationViewModel = ViewModelProviders.of(this).get(ExercisesRecommendationViewModel::class.java)

        configureHorizontalBarChart()

    }

    override fun onStart() {
        super.onStart()
        myBodyViewModel.getClients(this.applicationContext).observe(this, Observer { data ->
            if(data != null){
                data.setColors(colors, this.applicationContext)
                data.setDrawValues(false)
                barChart.data = BarData(data)
                barChart.invalidate()
            }
            else{
                Log.e(TAG, "Error while updating LiveData")
            }
        })

        exercisesRecommendationViewModel.getExercises(this.applicationContext).observe(this, Observer { exercises ->
            Log.d(TAG, "Exercises Been Observed")
            if(exercises != null){
                Log.d(TAG, "Exercises No Null - Success!!")
                adapter.setExercisesByCategory(exercises)
            }
            else{
                Log.e(TAG, "Error while updating Exercises LiveData List")
            }
        })
    }

    override fun onResume() {
        super.onResume()
        requestedOrientation =  ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    override fun onPause() {
        super.onPause()
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
    }

    override fun onStop() {
        super.onStop()
        myBodyViewModel.detachListeners()
    }


//------------------------------------------------------------------------
// SETUP FUNCTIONS
//------------------------------------------------------------------------

    /**
     * Helper function for back button in action bar
     */
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id: Int? = item?.itemId
        if(id == android.R.id.home){
            this.finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun configureHorizontalBarChart(){
        barChart = findViewById<HorizontalBarChart>(R.id.activity_mybody_graph)
        barChart.setMaxVisibleValueCount(40)
        barChart.setFitBars(true)

        barChart.xAxis.valueFormatter = MyBodyAxisValueFormatter(xLabels)
        barChart.xAxis.position = XAxis.XAxisPosition.BOTTOM
        barChart.xAxis.setDrawGridLines(false)

        barChart.axisLeft.setDrawAxisLine(false)
        barChart.axisLeft.setDrawGridLines(false)
        barChart.axisLeft.setDrawZeroLine(false)

        barChart.axisRight.setDrawAxisLine(false)
        barChart.axisRight.setDrawGridLines(false)
        barChart.axisRight.setDrawZeroLine(false)

    }
}
