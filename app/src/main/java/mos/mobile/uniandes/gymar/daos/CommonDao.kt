package mos.mobile.uniandes.gymar.daos

import android.arch.persistence.room.*

@Dao
interface CommonDao < in T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(t: T): Long

    @Delete
    fun delete(type : T)

    @Update
    fun update(type : T)
}