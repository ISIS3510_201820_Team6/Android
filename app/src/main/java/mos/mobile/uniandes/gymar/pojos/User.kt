package mos.mobile.uniandes.gymar.pojos

data class User(var name: String, var bodyFat: Double?, var weight: Double?, var maxHR: Int?, var imc: Double?, var age: Int?, var height: Int?, var image: String?)